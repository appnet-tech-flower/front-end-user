// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export var environment = {
    production: false,
    apiEndPoint: 'http://157.230.248.161:2000/',
    serverImage: 'http://157.230.248.161:3100/',
    staticImageSize: 'http://157.230.248.161:3100/images/using/',
    staticImageDemo: 'http://157.230.248.161:3100/images/temps/demo/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//# sourceMappingURL=environment.js.map