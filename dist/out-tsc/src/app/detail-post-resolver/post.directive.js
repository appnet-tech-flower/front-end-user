import * as tslib_1 from "tslib";
import { Directive, ViewContainerRef } from '@angular/core';
var PostDirective = /** @class */ (function () {
    function PostDirective(viewContainerRef) {
        this.viewContainerRef = viewContainerRef;
    }
    PostDirective = tslib_1.__decorate([
        Directive({
            selector: '[post-host]',
        }),
        tslib_1.__metadata("design:paramtypes", [ViewContainerRef])
    ], PostDirective);
    return PostDirective;
}());
export { PostDirective };
//# sourceMappingURL=post.directive.js.map