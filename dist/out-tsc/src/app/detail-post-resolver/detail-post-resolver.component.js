import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { BasePage } from '../shared/components/base/base-page';
import { PostDirective } from './post.directive';
var DetailPostResolverComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DetailPostResolverComponent, _super);
    function DetailPostResolverComponent(router, transferState, activatedRoute, componentFactoryResolver, httpClient) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.transferState = transferState;
        _this.activatedRoute = activatedRoute;
        _this.componentFactoryResolver = componentFactoryResolver;
        _this.httpClient = httpClient;
        _this.currentUrl = '';
        _this.detailObjects = {
        // projectList: new PostItem(ListProjectComponent, null),
        // projectDetail: new PostItem(DetailProjectComponent, null),
        // newsList: new PostItem(ListNewsComponent, null),
        // newsDetail: new PostItem(DetailNewsComponent, null)
        };
        _this.dataStateKey = makeStateKey('data');
        var sub = router.events.subscribe(function (event) {
            if (event instanceof NavigationEnd) {
                var url = event.urlAfterRedirects.split('?')[0];
                if (url && url[0] === '/') {
                    url = url.slice(1);
                    _this.currentUrl = url;
                }
                _this.callApiToDetectType(url);
            }
        });
        var sub1 = _this.activatedRoute.queryParams.subscribe(function (params) {
            _this.currentQueryParams = params;
        });
        _this.subscriptions.push(sub);
        _this.subscriptions.push(sub1);
        return _this;
    }
    DetailPostResolverComponent.prototype.callApiToDetectType = function (url) {
        var res = this.transferState.get(this.dataStateKey, null);
        if (res !== null) {
            this.loadComponent(res);
            this.transferState.remove(this.dataStateKey);
            return;
        }
        var referrer = document.referrer.toString();
        this.globalService.setLoadingSpinner(true);
        var params = tslib_1.__assign({ url: url,
            referrer: referrer }, this.currentQueryParams);
        console.log(params);
        // this.httpClient.get(APIs.getDataFromUrl, {params})
        //   .subscribe((res: any) => {
        //     if (res.status === HttpCode.SUCCESS) {
        //       this.loadComponent(res);
        //
        //       if (this.globalService.isPlatformServer) {
        //         this.transferState.set(this.dataStateKey, res);
        //       }
        //     } else {
        //       // redirect to home page
        //       this.router.navigate(['/']);
        //     }
        //
        //     this.globalService.setLoading(false);
        //   });
    };
    DetailPostResolverComponent.prototype.loadTypeComponent = function (data) {
        // switch (data.type) {
        //   case globalDefind.POST_TYPE_SALE:
        //     return data.isList ? this.detailObjects.postSaleList.component : this.detailObjects.postDetail.component;
        //   case globalDefind.POST_TYPE_BUY:
        //     return data.isList ? this.detailObjects.postBuyList.component : this.detailObjects.postDetail.component;
        //   case globalDefind.POST_TYPE_PROJECT:
        //     return data.isList ? this.detailObjects.projectList.component : this.detailObjects.projectDetail.component;
        //   case globalDefind.POST_TYPE_NEWS:
        //     return data.isList ? this.detailObjects.newsList.component : this.detailObjects.newsDetail.component;
        // }
        return null;
    };
    DetailPostResolverComponent.prototype.getUrlAndCustomUrl = function (data) {
        var result = {
            url: '',
            customUrl: ''
        };
        if (data.isList) {
            result.url = data.seo.url;
            result.customUrl = data.seo.customUrl;
        }
        else {
            result.url = data.data.url;
            result.customUrl = data.data.customUrl;
        }
        return result;
    };
    DetailPostResolverComponent.prototype.getUrlToBeRedirect = function (urlConfig) {
        var eleStrs = this.currentUrl.split('/');
        return eleStrs[0] + "/" + urlConfig.customUrl;
    };
    DetailPostResolverComponent.prototype.loadComponent = function (data) {
        var component = this.loadTypeComponent(data);
        if (component === null) {
            return;
        }
        var urlConfig = this.getUrlAndCustomUrl(data);
        if (urlConfig.customUrl && urlConfig.url !== urlConfig.customUrl) {
            var urlRedirectTo = this.getUrlToBeRedirect(urlConfig);
            if (urlRedirectTo !== this.currentUrl) {
                // redirect and keep all query parameters
                return this.router.navigate([urlRedirectTo], { queryParams: this.currentQueryParams });
            }
        }
        // Each component must have property "data"
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
        var viewContainerRef = this.postHost.viewContainerRef;
        viewContainerRef.clear();
        var componentRef = viewContainerRef.createComponent(componentFactory);
        // (<IPost>componentRef.instance).data = data;
    };
    tslib_1.__decorate([
        ViewChild(PostDirective),
        tslib_1.__metadata("design:type", PostDirective)
    ], DetailPostResolverComponent.prototype, "postHost", void 0);
    DetailPostResolverComponent = tslib_1.__decorate([
        Component({
            selector: 'app-detail-post-resolver',
            templateUrl: './detail-post-resolver.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            TransferState,
            ActivatedRoute,
            ComponentFactoryResolver,
            HttpClient])
    ], DetailPostResolverComponent);
    return DetailPostResolverComponent;
}(BasePage));
export { DetailPostResolverComponent };
//# sourceMappingURL=detail-post-resolver.component.js.map