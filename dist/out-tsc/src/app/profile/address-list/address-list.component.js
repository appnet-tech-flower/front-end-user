import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { ProfileService } from '../profile.service';
import { FormBuilder } from '@angular/forms';
import { SearchSelector } from '../../shared/constants/search-selector.constant';
var ListAddressComponent = /** @class */ (function (_super) {
    tslib_1.__extends(ListAddressComponent, _super);
    function ListAddressComponent(profileService, fb) {
        var _this = _super.call(this) || this;
        _this.profileService = profileService;
        _this.fb = fb;
        _this.listAddress = [];
        _this.display = false;
        _this.searchSelector = SearchSelector;
        _this.listDistrict = [];
        return _this;
    }
    ListAddressComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initForm(); // step 2
        this.globalService.setBreadcrumbs([
            {
                label: 'Trang chủ',
                icon: 'pi pi-home',
                routerLink: '/'
            },
            {
                label: 'Trang cá nhân',
                routerLink: '/trang-ca-nhan',
                icon: 'pi pi-user',
            },
            {
                label: 'Sổ địa ',
                routerLink: '/trang-ca-nhan/so-dia-chi',
                icon: 'pi pi-th-large'
            }
        ]);
        var sub = this.profileService.getAddressList()
            .subscribe(function (response) {
            _this.listAddress = response.data.addresses;
        });
        this.subscriptions.push(sub);
    };
    ListAddressComponent.prototype.edit = function (address) {
    };
    ListAddressComponent.prototype.remove = function (address) {
    };
    ListAddressComponent.prototype.addAddress = function () {
        this.display = true;
    };
    ListAddressComponent.prototype.changeCity = function (event) {
        this.listDistrict = event.value.districts;
    };
    ListAddressComponent.prototype.initForm = function () {
        this.form = this.fb.group({
            name: ['', [
                    this.validatorService.checkRequired()
                ]],
            phone: ['', [
                    this.validatorService.checkRequired()
                ]],
            city: ['', [
                    this.validatorService.checkRequired()
                ]],
            district: ['', [
                    this.validatorService.checkRequired()
                ]],
            address: ['', [
                    this.validatorService.checkRequired()
                ]]
        });
    };
    ListAddressComponent.prototype.resetAddAddressForm = function () {
        this.form.reset();
    };
    ListAddressComponent = tslib_1.__decorate([
        Component({
            selector: 'app-profile-address-list',
            templateUrl: './address-list.component.html',
            styleUrls: ['./address-list.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ProfileService,
            FormBuilder])
    ], ListAddressComponent);
    return ListAddressComponent;
}(BasePage));
export { ListAddressComponent };
//# sourceMappingURL=address-list.component.js.map