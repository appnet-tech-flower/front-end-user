import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NewProductComponent } from './new-product/new-product.component';
import { ProfileComponent } from './profile.component';
import { ListAddressComponent } from './address-list/address-list.component';
import { NotificationComponent } from './notification/notification.component';
import { UpdateInfoUserComponent } from './update-info-user/update-info-user.component';
var ProfileRoutingModule = /** @class */ (function () {
    function ProfileRoutingModule() {
    }
    ProfileRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forChild([
                    {
                        path: '',
                        component: ProfileComponent,
                        children: [
                            {
                                path: 'dang-san-pham',
                                component: NewProductComponent
                            },
                            {
                                path: 'so-dia-chi',
                                component: ListAddressComponent
                            },
                            {
                                path: 'thong-bao',
                                component: NotificationComponent
                            },
                            {
                                path: 'thong-tin-ca-nhan',
                                component: UpdateInfoUserComponent
                            }
                        ]
                    }
                ])],
            exports: [RouterModule]
        })
    ], ProfileRoutingModule);
    return ProfileRoutingModule;
}());
export { ProfileRoutingModule };
//# sourceMappingURL=profile-routing.module.js.map