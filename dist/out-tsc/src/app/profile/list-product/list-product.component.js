import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ListProductComponent = /** @class */ (function () {
    function ListProductComponent() {
    }
    ListProductComponent = tslib_1.__decorate([
        Component({
            selector: 'app-profile-list-product',
            templateUrl: './list-product.component.html',
            styleUrls: ['./list-product.component.scss']
        })
    ], ListProductComponent);
    return ListProductComponent;
}());
export { ListProductComponent };
//# sourceMappingURL=list-product.component.js.map