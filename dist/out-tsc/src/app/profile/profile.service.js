import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
var ProfileService = /** @class */ (function () {
    function ProfileService(httpClient) {
        this.httpClient = httpClient;
    }
    ProfileService.prototype.getAddressList = function () {
        return of({
            status: 1,
            messages: ['Successfully'],
            data: {
                addresses: [
                    {
                        name: 'Nguyễn Văn A',
                        address: 'Gò Vấp',
                        phone: '19001515'
                    },
                    {
                        name: 'Nguyễn Văn B',
                        address: 'Bình Thạnh',
                        phone: '19001516'
                    },
                    {
                        name: 'Nguyễn Văn C',
                        address: 'Quận 10',
                        phone: '19001517'
                    },
                    {
                        name: 'Nguyễn Văn D',
                        address: 'Quận 11',
                        phone: '19001518'
                    }
                ]
            }
        });
        // return this.httpClient.get(API.Profile.AddressList);
    };
    ProfileService.prototype.getNotificationList = function () {
        return of({
            status: 1,
            messages: ['Successfully'],
            data: {
                meta: {
                    totalItems: 50
                },
                entries: [
                    {
                        time: '20/11/2019',
                        // tslint:disable-next-line:max-line-length
                        content: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
                    },
                    {
                        time: '21/11/2019',
                        // tslint:disable-next-line:max-line-length
                        content: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
                    },
                    {
                        time: '22/11/2019',
                        // tslint:disable-next-line:max-line-length
                        content: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
                    },
                    {
                        time: '23/11/2019',
                        // tslint:disable-next-line:max-line-length
                        content: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
                    },
                    {
                        time: '24/11/2019',
                        // tslint:disable-next-line:max-line-length
                        content: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
                    },
                    {
                        time: '25/11/2019',
                        // tslint:disable-next-line:max-line-length
                        content: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
                    },
                ]
            }
        });
        // return this.httpClient.get(API.Profile.AddressList);
    };
    ProfileService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], ProfileService);
    return ProfileService;
}());
export { ProfileService };
//# sourceMappingURL=profile.service.js.map