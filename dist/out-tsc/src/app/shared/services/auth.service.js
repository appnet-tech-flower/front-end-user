import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
var AuthService = /** @class */ (function () {
    function AuthService() {
        this.userInfo$ = new BehaviorSubject(null);
    }
    AuthService.prototype.setUserInfo = function (value) {
        this.userInfo = value;
        this.userInfo$.next(value);
    };
    AuthService.prototype.getUserInfo = function () {
        return this.userInfo$.asObservable();
    };
    AuthService = tslib_1.__decorate([
        Injectable()
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map