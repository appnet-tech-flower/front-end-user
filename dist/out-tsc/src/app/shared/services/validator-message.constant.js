export var Validator;
(function (Validator) {
    var Code;
    (function (Code) {
        Code.Required = 'required';
        Code.MinLength = 'minLength';
        Code.MaxLength = 'maxLength';
        Code.WrongPatternNumber = 'wrongPatternNumber';
    })(Code = Validator.Code || (Validator.Code = {}));
    var Message;
    (function (Message) {
        Message.Required = 'Trường bắt buộc';
        Message.MinLength = 'Quá ngắn. Ít nhất {value} kí tự';
        Message.MaxLength = 'Quá dài. Tối đa {value} kí tự';
        Message.WrongPatternNumber = 'Giá trị phải là chuỗi số';
    })(Message = Validator.Message || (Validator.Message = {}));
})(Validator || (Validator = {}));
//# sourceMappingURL=validator-message.constant.js.map