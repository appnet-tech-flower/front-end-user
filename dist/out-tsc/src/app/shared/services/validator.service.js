import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Validator } from './validator-message.constant';
var ValidatorService = /** @class */ (function () {
    function ValidatorService() {
    }
    ValidatorService_1 = ValidatorService;
    ValidatorService.isUndefined = function (value) {
        return value === undefined;
    };
    ValidatorService.isNull = function (value) {
        return value === null;
    };
    ValidatorService.prototype.checkRequired = function () {
        return function (control) {
            var errors = {};
            var value = control.value || '';
            if (ValidatorService_1.isNull(value) ||
                ValidatorService_1.isUndefined(value) ||
                value === '') {
                errors[Validator.Code.Required] = Validator.Message.Required;
            }
            return errors;
        };
    };
    ValidatorService.prototype.checkMinLength = function (minLength) {
        return function (control) {
            var errors = {};
            var value = control.value || '';
            if (value.toString().length < minLength) {
                errors[Validator.Code.MinLength] = Validator.Message.MinLength.replace('{value}', minLength.toString());
            }
            return errors;
        };
    };
    ValidatorService.prototype.checkMaxLength = function (maxlength) {
        return function (control) {
            var errors = {};
            var value = control.value || '';
            if (value.toString().length > maxlength) {
                errors[Validator.Code.MaxLength] = Validator.Message.MaxLength.replace('{value}', maxlength.toString());
            }
            return errors;
        };
    };
    var ValidatorService_1;
    ValidatorService = ValidatorService_1 = tslib_1.__decorate([
        Injectable()
    ], ValidatorService);
    return ValidatorService;
}());
export { ValidatorService };
//# sourceMappingURL=validator.service.js.map