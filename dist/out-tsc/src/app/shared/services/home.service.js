import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../constants/api.constant';
var HomeService = /** @class */ (function () {
    function HomeService(_http) {
        this._http = _http;
    }
    HomeService.prototype.getProduct = function () {
        return this._http.get(API.Home.Get);
    };
    HomeService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], HomeService);
    return HomeService;
}());
export { HomeService };
//# sourceMappingURL=home.service.js.map