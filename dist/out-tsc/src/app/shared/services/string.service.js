import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
var StringService = /** @class */ (function () {
    function StringService() {
    }
    StringService.guidGenerator = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) || 0).toString(32).substring(1);
        };
        return S4() + "_" + S4() + "_" + new Date().getTime();
    };
    StringService = tslib_1.__decorate([
        Injectable()
    ], StringService);
    return StringService;
}());
export { StringService };
//# sourceMappingURL=string.service.js.map