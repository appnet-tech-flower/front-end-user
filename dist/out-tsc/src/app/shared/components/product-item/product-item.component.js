import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import * as moment from "moment";
var ProductItemComponent = /** @class */ (function () {
    function ProductItemComponent() {
        this.showSaleOff = false;
        //dump image
        this.images = [
            "/assets/demo/img-1.jpg",
            "/assets/demo/img-2.jpg"
        ];
    }
    ProductItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.startDate = moment(this.product.saleOff.startDate).unix();
        this.endDate = moment(this.product.saleOff.endDate).unix();
        var diff = this.endDate - this.startDate;
        var duration = moment.duration(diff * 1000, 'milliseconds');
        var interval = 1000;
        setInterval(function () {
            duration = moment.duration(duration.asMilliseconds() - interval, 'milliseconds');
            _this.day = moment.duration(duration).days();
            _this.hour = moment.duration(duration).hours();
            _this.minute = moment.duration(duration).minutes();
            _this.second = moment.duration(duration).seconds();
            _this.countdown = _this.startDate * 100 / (_this.startDate + _this.endDate);
        }, interval);
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ProductItemComponent.prototype, "product", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], ProductItemComponent.prototype, "showSaleOff", void 0);
    ProductItemComponent = tslib_1.__decorate([
        Component({
            selector: 'app-product-item',
            templateUrl: './product-item.component.html',
            styleUrls: ['./product-item.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ProductItemComponent);
    return ProductItemComponent;
}());
export { ProductItemComponent };
//# sourceMappingURL=product-item.component.js.map