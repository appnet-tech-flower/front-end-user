import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TabViewModule, ButtonModule } from 'primeng/primeng';
import { ProgressBarModule } from 'primeng/progressbar';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductItemComponent } from './product-item.component';
var ProductItemModule = /** @class */ (function () {
    function ProductItemModule() {
    }
    ProductItemModule = tslib_1.__decorate([
        NgModule({
            declarations: [ProductItemComponent],
            imports: [
                CommonModule,
                HttpClientModule,
                TabViewModule,
                ReactiveFormsModule,
                ButtonModule,
                ProgressBarModule
            ],
            exports: [
                ProductItemComponent
            ],
            providers: []
        })
    ], ProductItemModule);
    return ProductItemModule;
}());
export { ProductItemModule };
//# sourceMappingURL=product-item.module.js.map