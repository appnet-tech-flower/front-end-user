import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { SearchSelector } from "../../constants/search-selector.constant";
var Topics = SearchSelector.Topics;
var SpecialOccasions = SearchSelector.SpecialOccasions;
var Designs = SearchSelector.Designs;
var Florets = SearchSelector.Florets;
var Colors = SearchSelector.Colors;
var PriceRanges = SearchSelector.PriceRanges;
var Cities = SearchSelector.Cities;
var SearchBoxComponent = /** @class */ (function () {
    function SearchBoxComponent(_fb) {
        this._fb = _fb;
        this.isFreshFlower = false;
        this.topics = Topics;
        this.specials = SpecialOccasions;
        this.designs = Designs;
        this.florets = Florets;
        this.colors = Colors;
        this.price = PriceRanges;
        this.cities = Cities;
        this.districts = [];
        this.searchForm = this._fb.group({
            topic: [''],
            specialOccasion: [''],
            design: [''],
            floret: [''],
            color: [''],
            price: [''],
            city: [''],
            district: ['']
        });
    }
    SearchBoxComponent.prototype.ngOnInit = function () { };
    SearchBoxComponent.prototype.onSearch = function () {
        console.log(this.searchForm.value);
    };
    SearchBoxComponent.prototype.onChangeCity = function (event) {
        if (event) {
            this.districts = this.cities.find(function (city) { return city.code == event.code; }).districts;
        }
        if (!event) {
            this.districts = [];
        }
    };
    SearchBoxComponent.prototype.onTopicChanged = function (event) {
        if (event && event.value === 11) {
            this.isFreshFlower = true;
        }
        else {
            this.isFreshFlower = false;
        }
    };
    SearchBoxComponent = tslib_1.__decorate([
        Component({
            selector: "app-search-box",
            templateUrl: "./search-box.component.html",
            styleUrls: ["./search-box.component.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder])
    ], SearchBoxComponent);
    return SearchBoxComponent;
}());
export { SearchBoxComponent };
//# sourceMappingURL=search-box.component.js.map