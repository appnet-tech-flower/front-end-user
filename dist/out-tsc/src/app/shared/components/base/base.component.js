import * as tslib_1 from "tslib";
import { HostBinding, Input } from '@angular/core';
import { StringService } from '../../services/string.service';
var BaseComponent = /** @class */ (function () {
    function BaseComponent() {
        this.id = StringService.guidGenerator();
        this.classes = '';
        this.errors = [];
        this.style = {};
        this.isDisabled = false;
    }
    tslib_1.__decorate([
        Input(),
        HostBinding('attr.data-id'),
        tslib_1.__metadata("design:type", Object)
    ], BaseComponent.prototype, "id", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], BaseComponent.prototype, "classes", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], BaseComponent.prototype, "errors", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], BaseComponent.prototype, "style", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], BaseComponent.prototype, "isDisabled", void 0);
    return BaseComponent;
}());
export { BaseComponent };
//# sourceMappingURL=base.component.js.map