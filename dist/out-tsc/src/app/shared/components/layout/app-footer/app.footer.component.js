import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var AppFooterComponent = /** @class */ (function () {
    function AppFooterComponent() {
    }
    AppFooterComponent = tslib_1.__decorate([
        Component({
            selector: 'app-footer',
            templateUrl: './app.footer.component.html',
            styleUrls: ['./app-footer.component.scss']
        })
    ], AppFooterComponent);
    return AppFooterComponent;
}());
export { AppFooterComponent };
//# sourceMappingURL=app.footer.component.js.map