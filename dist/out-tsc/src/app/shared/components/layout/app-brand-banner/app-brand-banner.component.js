import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var AppBrandBannerComponent = /** @class */ (function () {
    function AppBrandBannerComponent() {
        this.img = '/assets/demo/top-banner.jpg';
    }
    AppBrandBannerComponent = tslib_1.__decorate([
        Component({
            selector: 'app-brand-banner',
            templateUrl: './app-brand-banner.component.html',
            styleUrls: ['./app-brand-banner.component.scss']
        })
    ], AppBrandBannerComponent);
    return AppBrandBannerComponent;
}());
export { AppBrandBannerComponent };
//# sourceMappingURL=app-brand-banner.component.js.map