import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Component, DebugElement, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { API } from '../../constants/api.constant';
import { GlobalConstant } from '../../constants/global.constant';
import { StringService } from '../../services/string.service';
import { BaseComponent } from '../base/base.component';
var IMAGE_UPLOADER_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(function () { return ImageUploaderComponent; }),
    multi: true
};
var ImageUploaderComponent = /** @class */ (function (_super) {
    tslib_1.__extends(ImageUploaderComponent, _super);
    function ImageUploaderComponent(el, http) {
        var _this = _super.call(this) || this;
        _this.el = el;
        _this.http = http;
        _this._files = [];
        _this._values = [];
        _this.GlobalConstant = GlobalConstant;
        _this.environment = environment;
        _this.max = -1;
        _this.maxSize = 1024 * 2; // 2mb
        _this.valueChanged = new EventEmitter();
        _this.onModelChange = function (_) {
        };
        _this.onModelTouched = function () {
        };
        return _this;
    }
    Object.defineProperty(ImageUploaderComponent.prototype, "values", {
        get: function () {
            return this._values;
        },
        set: function (values) {
            var _this = this;
            this._values = values;
            this._files = [];
            values.forEach(function (link) {
                if (link.isDemo === undefined) {
                    link.isDemo = true;
                }
                _this._files.push({
                    elId: StringService.guidGenerator(),
                    file: {},
                    percentUploaded: 0,
                    result: {
                        link: link.link
                    },
                    url: _this.getLinkImage(link),
                    isDemo: link.isDemo,
                });
            });
        },
        enumerable: true,
        configurable: true
    });
    ImageUploaderComponent.prototype.ngAfterContentInit = function () {
    };
    ImageUploaderComponent.prototype.writeValue = function (obj) {
        this.values = obj;
    };
    ImageUploaderComponent.prototype.registerOnChange = function (fn) {
        this.onModelChange = fn;
    };
    ImageUploaderComponent.prototype.registerOnTouched = function (fn) {
        this.onModelTouched = fn;
    };
    ImageUploaderComponent.prototype.updateModel = function () {
        this.values = this._files
            .map(function (f) {
            return {
                link: f.result ? f.result.link : '',
                isDemo: f.isDemo
            };
        });
        this.valueChanged.emit(this.values);
        this.onModelChange(this.values);
    };
    ImageUploaderComponent.prototype.onChangeInputFiles = function (event) {
        var _this = this;
        var selectedFiles = event.target.files;
        if (selectedFiles.length === 0) {
            return;
        }
        if (this.max !== -1 && this._files.length + selectedFiles.length > this.max) {
            this.resetValueOfInput();
            return;
        }
        if (this._existFileTooLarge(selectedFiles)) {
            this.resetValueOfInput();
            return;
        }
        var waitingImages = this.generateObjectImage(selectedFiles);
        this._files = this._files.concat(waitingImages);
        if (waitingImages.length > 0) {
            setTimeout(function () {
                _this._renderNewImages(waitingImages);
            }, 100);
            this.resetValueOfInput();
        }
    };
    ImageUploaderComponent.prototype.trackBy = function (index, item) {
        return item.elId;
    };
    ImageUploaderComponent.prototype.removeFile = function (file) {
        var index = this.findIndexFile(file);
        if (index !== -1) {
            this._files.splice(index, 1);
            this.updateModel();
        }
    };
    ImageUploaderComponent.prototype.generateObjectImage = function (files) {
        var results = [];
        for (var key in files) {
            if (files.hasOwnProperty(key)) {
                results.push({
                    elId: StringService.guidGenerator(),
                    file: files[key],
                    percentUploaded: 0,
                    result: null,
                    url: ''
                });
            }
        }
        return results;
    };
    ImageUploaderComponent.prototype.findIndexFile = function (file) {
        return this._files.findIndex(function (f) { return f.elId === file.elId; });
    };
    ImageUploaderComponent.prototype.resetValueOfInput = function () {
        this.inputFile.nativeElement.value = '';
    };
    ImageUploaderComponent.prototype._renderNewImages = function (newImages) {
        var _this = this;
        newImages.forEach(function (imgFile) {
            _this._uploadImage(imgFile);
        });
    };
    ImageUploaderComponent.prototype._existFileTooLarge = function (selectedFiles) {
        // TODO:
        return false;
    };
    ImageUploaderComponent.prototype._uploadImage = function (imgFile) {
        var _this = this;
        var index = this.findIndexFile(imgFile);
        if (index === -1) {
            return;
        }
        var formData = new FormData();
        formData.append('file', imgFile.file);
        this.http.post(API.uploadImage, formData)
            .subscribe(function (res) {
            if (res.status === 1) {
                _this._files[index].url = environment.staticImageDemo + res.data.link.toString();
                _this._files[index].result = res.data;
                _this._files[index].isDemo = true;
                _this.updateModel();
            }
            console.log(_this._files);
        });
    };
    ImageUploaderComponent.prototype.getLinkImage = function (link) {
        if (link.link.indexOf('http://') >= 0 || link.link.indexOf('https://') >= 0) {
            return link.link;
        }
        return link.isDemo ?
            environment.staticImageDemo + link.link :
            environment.staticImageSize + GlobalConstant.SizeImage.S164x170 + '/' + link.link;
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array),
        tslib_1.__metadata("design:paramtypes", [Array])
    ], ImageUploaderComponent.prototype, "values", null);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ImageUploaderComponent.prototype, "max", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ImageUploaderComponent.prototype, "maxSize", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], ImageUploaderComponent.prototype, "valueChanged", void 0);
    tslib_1.__decorate([
        ViewChild('inputFile'),
        tslib_1.__metadata("design:type", DebugElement)
    ], ImageUploaderComponent.prototype, "inputFile", void 0);
    ImageUploaderComponent = tslib_1.__decorate([
        Component({
            templateUrl: './imageUploader.component.html',
            selector: 'app-image-uploader',
            styleUrls: ['./imageUploader.component.scss'],
            providers: [IMAGE_UPLOADER_VALUE_ACCESSOR]
        }),
        tslib_1.__metadata("design:paramtypes", [ElementRef,
            HttpClient])
    ], ImageUploaderComponent);
    return ImageUploaderComponent;
}(BaseComponent));
export { ImageUploaderComponent };
//# sourceMappingURL=imageUploader.component.js.map