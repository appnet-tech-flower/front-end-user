export var GlobalConstant;
(function (GlobalConstant) {
    var SizeImage;
    (function (SizeImage) {
        SizeImage.S120x90 = '120x90';
        SizeImage.S200x200 = '200x200';
        SizeImage.S82x82 = '82x82';
        SizeImage.S150x150 = '150x150';
        SizeImage.S745x510 = '745x510';
        SizeImage.S255x180 = '255x180';
        SizeImage.S640x430 = '640x430';
        SizeImage.S164x170 = '164x170';
    })(SizeImage = GlobalConstant.SizeImage || (GlobalConstant.SizeImage = {}));
})(GlobalConstant || (GlobalConstant = {}));
//# sourceMappingURL=global.constant.js.map