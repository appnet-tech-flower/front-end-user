export var HTTP_CODES;
(function (HTTP_CODES) {
    HTTP_CODES.SUCCESS = 1;
    HTTP_CODES.ERROR = 0;
    HTTP_CODES.ERROR_AUTHORIZED = 401;
})(HTTP_CODES || (HTTP_CODES = {}));
//# sourceMappingURL=http-codes.constant.js.map