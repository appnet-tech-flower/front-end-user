import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DemoFormComponent } from './demo-form/demo-form.component';
var routes = [
    {
        path: '',
        pathMatch: 'full',
        component: DemoFormComponent
    }
];
var DemoRoutingModule = /** @class */ (function () {
    function DemoRoutingModule() {
    }
    DemoRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], DemoRoutingModule);
    return DemoRoutingModule;
}());
export { DemoRoutingModule };
//# sourceMappingURL=demo-routing.module.js.map