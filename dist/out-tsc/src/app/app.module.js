import * as tslib_1 from "tslib";
import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BreadcrumbModule, ProgressSpinnerModule } from 'primeng/primeng';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { AppLayoutModule } from './shared/components/layout/app-layout.module';
import { AuthService } from './shared/services/auth.service';
import { GlobalService } from './shared/services/global.service';
import { ServiceLocator } from './shared/services/locator.service';
import { ValidatorService } from './shared/services/validator.service';
import { AuthModule } from './auth/auth.module';
import { SearchResultsModule } from './search-results/search-results.module';
import { DetailProductModule } from './detail-product/detail-product.module';
var AppModule = /** @class */ (function () {
    function AppModule(injector) {
        this.injector = injector;
        ServiceLocator.injector = injector;
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                AppComponent
            ],
            imports: [
                BrowserModule.withServerTransition({ appId: 'serverApp' }),
                NoopAnimationsModule,
                AppRoutingModule,
                ProgressSpinnerModule,
                AppLayoutModule,
                HomeModule,
                HomeModule,
                AuthModule,
                BreadcrumbModule,
                SearchResultsModule,
                DetailProductModule
            ],
            providers: [
                ServiceLocator,
                ValidatorService,
                AuthService,
                GlobalService
            ],
            bootstrap: [AppComponent]
        }),
        tslib_1.__metadata("design:paramtypes", [Injector])
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map