import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HomeService } from '../../../shared/services/home.service';
var ProductsPromotionComponent = /** @class */ (function () {
    function ProductsPromotionComponent(_homeService) {
        this._homeService = _homeService;
    }
    ProductsPromotionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._homeService.getProduct().subscribe(function (res) {
            _this.saleProducts = res.data.entries.saleProducts;
        });
    };
    ProductsPromotionComponent = tslib_1.__decorate([
        Component({
            selector: 'app-products-promotion',
            templateUrl: './products-promotion.component.html',
            styleUrls: ['./products-promotion.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [HomeService])
    ], ProductsPromotionComponent);
    return ProductsPromotionComponent;
}());
export { ProductsPromotionComponent };
//# sourceMappingURL=products-promotion.component.js.map