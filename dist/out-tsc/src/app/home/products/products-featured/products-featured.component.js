import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { HomeService } from '../../../shared/services/home.service';
var ProductsFeaturedComponent = /** @class */ (function () {
    function ProductsFeaturedComponent(_homeService) {
        this._homeService = _homeService;
    }
    ProductsFeaturedComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._homeService.getProduct().subscribe(function (res) {
            _this.featuredProducts = res.data.entries.featuredProducts;
        });
    };
    ProductsFeaturedComponent = tslib_1.__decorate([
        Component({
            selector: 'app-products-featured',
            templateUrl: './products-featured.component.html',
            styleUrls: ['./products-featured.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [HomeService])
    ], ProductsFeaturedComponent);
    return ProductsFeaturedComponent;
}());
export { ProductsFeaturedComponent };
//# sourceMappingURL=products-featured.component.js.map