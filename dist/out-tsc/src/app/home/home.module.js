import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { ProductsModule } from './products/products.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = tslib_1.__decorate([
        NgModule({
            declarations: [HomeComponent],
            imports: [
                CommonModule,
                ProductsModule,
                SearchBoxModule
            ],
            exports: [HomeComponent]
        })
    ], HomeModule);
    return HomeModule;
}());
export { HomeModule };
//# sourceMappingURL=home.module.js.map