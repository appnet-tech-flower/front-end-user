import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { TabMenuModule } from 'primeng/tabmenu';
import { ButtonModule, InputTextModule, MessageModule, MessagesModule, PanelMenuModule, TabViewModule } from 'primeng/primeng';
import { ToastModule } from 'primeng/toast';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PopupComponent } from './popup/popup.component';
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = tslib_1.__decorate([
        NgModule({
            declarations: [LoginComponent, RegisterComponent, PopupComponent],
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                ToastModule,
                MessagesModule,
                MessageModule,
                ButtonModule,
                InputTextModule,
                DynamicDialogModule,
                TabMenuModule,
                PanelMenuModule,
                TabViewModule
            ],
            entryComponents: [LoginComponent, RegisterComponent, PopupComponent]
        })
    ], AuthModule);
    return AuthModule;
}());
export { AuthModule };
//# sourceMappingURL=auth.module.js.map