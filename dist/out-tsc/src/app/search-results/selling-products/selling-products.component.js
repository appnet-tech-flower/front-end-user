import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Products } from '../../shared/constants/products.constant';
var SellingProductsComponent = /** @class */ (function () {
    function SellingProductsComponent() {
    }
    SellingProductsComponent.prototype.ngOnInit = function () {
        this.products = Products;
    };
    SellingProductsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-selling-products',
            templateUrl: './selling-products.component.html',
            styleUrls: ['./selling-products.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SellingProductsComponent);
    return SellingProductsComponent;
}());
export { SellingProductsComponent };
//# sourceMappingURL=selling-products.component.js.map