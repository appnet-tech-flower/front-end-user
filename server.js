const express = require('express');
const app = express();

app.use(express.static('dist/browser'));

app.get('/', (req, res) => {
  res.render('dist/browser/index.html');
});

const PORT = 4000;

app.listen(PORT, () => {
  console.log(`Flower user is running on port ${PORT}`);
});
