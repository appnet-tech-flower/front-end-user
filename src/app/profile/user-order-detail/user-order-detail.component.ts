import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from '../profile.service';
import { environment } from '../../../environments/environment';
import { GlobalConstant } from '../../shared/constants/global.constant';

export interface IResOrderDetail {
  status: number;
  messages: string[];
  data: OrderDetail[];
}

export interface OrderDetail {
  quantity: number;
  price: number;
  status: number;
  shop: {
    name: string;
  };
  title: string;
  images: string[];
}

@Component({
  selector: 'app-user-order-detail-component',
  templateUrl: './user-order-detail.component.html',
  styleUrls: ['./user-order-detail.component.scss']
})

export class UserOrderDetailComponent extends BasePage implements OnInit {
  orderDetails: OrderDetail[];
  orderId: string;
  environment = environment;
  SizeImage = GlobalConstant.SizeImage;
  status = GlobalConstant.Status;
  constructor(private activatedRoute: ActivatedRoute,
              private profileService: ProfileService) {
    super();
    activatedRoute.params.subscribe(params => {
      this.orderId = params.id;
    });
  }
  ngOnInit(): void {
    this.getOrderDetail();
  }

  getOrderDetail() {
    const sub = this.profileService.getOrderDetail(this.orderId)
      .subscribe((response: IResOrderDetail) => {
        this.orderDetails = response.data;
      });
    this.subscriptions.push(sub);
  }
}
