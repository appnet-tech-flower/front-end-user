import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ButtonModule,
  CalendarModule,
  CheckboxModule,
  CardModule,
  ChipsModule,
  ConfirmationService,
  ConfirmDialogModule,
  DialogModule,
  DropdownModule,
  InputMaskModule,
  InputTextareaModule,
  MessagesModule,
  InputTextModule,
  ToolbarModule,
  TabViewModule,
  PaginatorModule,
  RadioButtonModule,
  PasswordModule,
  InputSwitchModule, MessageModule, EditorModule, KeyFilterModule
} from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { ImageUploaderModule } from '../shared/components/image-uploader/imageUploader.module';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';
import { ListAddressComponent } from './address-list/address-list.component';
import { NotificationComponent } from './notification/notification.component';
import { UpdateInfoUserComponent } from './update-info-user/update-info-user.component';
import { TokenInterceptor } from '../shared/services/token-interceptor.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { RegisterShopComponent } from './register-shop/register-shop.component';
import { RechargeComponent } from './recharge/recharge.component';
import { UserOrderListComponent } from './user-order-list/user-order-list.component';
import { UserOrderDetailComponent } from './user-order-detail/user-order-detail.component';
import { MapModule } from '../shared/components/map/map.module';
import { AppStaticImageModule } from "../shared/pipes/app-static-image/app-static-image.module";
@NgModule({
  declarations: [
    ProfileComponent,
    ListAddressComponent,
    NotificationComponent,
    UpdateInfoUserComponent,
    RegisterShopComponent,
    RechargeComponent,
    UserOrderListComponent,
    UserOrderDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
    MessagesModule,
    MessageModule,
    InputMaskModule,
    InputTextareaModule,
    ChipsModule,
    ButtonModule,
    ImageUploaderModule,
    CardModule,
    InputTextModule,
    DropdownModule,
    ButtonModule,
    TableModule,
    ToolbarModule,
    TabViewModule,
    PaginatorModule,
    ToolbarModule,
    DialogModule,
    RadioButtonModule,
    CalendarModule,
    PasswordModule,
    CheckboxModule,
    ToastModule,
    ConfirmDialogModule,
    InputSwitchModule,
    EditorModule,
    KeyFilterModule,
    MapModule,
    AppStaticImageModule
  ],
  providers: [
    ProfileService,
    DecimalPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    ConfirmationService
  ],
  exports: []
})
export class ProfileModule {

}
