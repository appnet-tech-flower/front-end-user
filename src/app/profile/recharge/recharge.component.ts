import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { AuthService } from '../../shared/services/auth.service';
import { GlobalService } from '../../shared/services/global.service';
import { SeoService } from '../../shared/services/seo.service';

interface IUser {
  _id: string;
  role: number;
  email: string;
  username: string;
  name: string;
  phone: string;
  address?: string;
  type: number;
  status: number;
  gender: number;
  district?: number;
  ward?: number;
  registerBy?: number;
}

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.scss'],
})

export class RechargeComponent extends BasePage implements OnInit {

  user: IUser;

  hecta = {
    name: 'Văn Đức Sơn Hà',
    account_number_acb: '47718749',
    account_number_donga: '0101097130'
  };

  srcACB: string = GlobalConstant.Bank.ACB;
  srcDongA: string = GlobalConstant.Bank.DONGA;

  constructor(private _authService: AuthService,
              public globalService: GlobalService,
              private seoService: SeoService) {
    super();
  }

  ngOnInit() {
    this.getUser();
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Trang cá nhân',
        routerLink: '/trang-ca-nhan',
        icon: 'pi pi-user',
      },
      {
        label: 'Nạp tiền vào tài khoản',
        routerLink: '/trang-ca-nhan/nap-tien-vao-tai-khoan',
        icon: 'pi pi-dollar'
      }
    ]);
    this.seoService.setHeader('Trang cá nhân - Nạp tiền vào tài khoản');
  }

  getUser() {
    const sub = this._authService.getUserInfo().subscribe(
      (res: any) => {
        this.user = res;
      }
    );

    this.subscriptions.push(sub);
  }

}
