import { Component, OnInit, ViewChild } from '@angular/core';
import { Dialog } from 'primeng/dialog';
import { debounce, debounceTime, delay, distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { BasePage } from '../../shared/components/base/base-page';
import { IMap } from '../../shared/components/map/map.component';
import { SeoService } from '../../shared/services/seo.service';
import { ProfileService } from '../profile.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SearchSelector } from '../../shared/constants/search-selector.constant';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { ConfirmationService } from 'primeng/api';

interface IAddress {
  _id: string;
  name: string;
  phone: string;
  city: string;
  district: number;
  ward: number;
  address: string;
  addressText: any;
}

export interface IResAddressList {
  status: number;
  messages: string[];
  data: {
    meta: {},
    entries: IAddress[]
  };
}

export interface IBody {
  name?: string;
  phone?: string;
  city?: string;
  district?: number;
  ward?: number;
  address?: string;
  longitude?: number;
  latitude?: number;
}

@Component({
  selector: 'app-profile-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss'],
})

export class ListAddressComponent extends BasePage implements OnInit {
  form: FormGroup;
  listAddress: IAddress[] = [];
  display = false;
  searchSelector = SearchSelector;
  addressBody: IBody = {};
  addressId: string = null;
  addressText = 'Ho Chinh Minh, Viet Nam';
  googleAddress: '';
  isMapLoading = false;
  @ViewChild('addressDialog') addressDialog: Dialog;
  private _formValue: {} = null;

  constructor(private profileService: ProfileService,
              private fb: FormBuilder,
              private confirmationService: ConfirmationService,
              private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    this.initForm();
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Trang cá nhân',
        routerLink: '/trang-ca-nhan',
        icon: 'pi pi-user',
      },
      {
        label: 'Sổ địa ',
        routerLink: '/trang-ca-nhan/so-dia-chi',
        icon: 'pi pi-th-large'
      }
    ]);
    this.fetchData();
    this.seoService.setHeader('Trang cá nhân - Sổ địa chỉ');
  }

  onSubmit() {
    const countError = this.getNumberOfFormErrors(this.form);
    if (countError === 0) {
      const tempAddress = this.form.getRawValue();
      Object.assign(this.addressBody, tempAddress);
      if (this.addressId === null) {
        this.addAddress(this.addressBody);
      } else {
        this.changeAddress(this.addressBody, this.addressId);
      }
    }
  }

  addAddress(addressBody: IBody) {
    this.globalService.setLoadingSpinner(true);
    const sub = this.profileService.addNewAddress(addressBody)
        .subscribe((response: IResAddressList) => {
          if (response.status === HTTP_CODES.SUCCESS) {
            this.display = false;
          }

          this.addressText = response.data.entries[0].addressText;
          this.initMessage(response, 'Thêm địa chỉ mới thành công');
          this.globalService.setLoadingSpinner(false);
        });
    this.subscriptions.push(sub);
  }

  onMapChanged(value: IMap | null) {
    if (value) {
      this.form.controls.address.setValue(value.address);
      this.form.controls.longitude.setValue(value.longitude);
      this.form.controls.latitude.setValue(value.latitude);
    }
  }

  changeAddress(addressBody: IBody, addressId: string) {
    this.globalService.setLoadingSpinner(true);
    const sub = this.profileService.changeAddress(addressBody, addressId)
        .subscribe((response: IResAddressList) => {
          if (response.status === HTTP_CODES.SUCCESS) {
            this.display = false;
          }

          this.addressText = response.data.entries[0].addressText;
          this.initMessage(response, 'Cập nhật thông tin thành công');
          this.globalService.setLoadingSpinner(false);
        });
    this.subscriptions.push(sub);
  }

  removeAddress(addressId: string) {
    this.confirmationService.confirm({
      message: 'Bạn có muốn xoá địa chỉ này ?',
      accept: () => {
        const sub = this.profileService.removeAddress(addressId)
            .subscribe((response: IResAddressList) => {
              this.initMessage(response, 'Xoá địa chỉ thành công');
            });
        this.subscriptions.push(sub);
      }
    });
  }

  initMessage(response: IResAddressList, successfulDetail: string) {
    if (response.status === HTTP_CODES.SUCCESS) {
      this.messageService.add({severity: 'success', summary: 'Thông Báo', detail: successfulDetail, life: 10000});
      this.fetchData();
    } else {
      let errorMessage = '';
      response.messages.forEach(event => {
        errorMessage += event + '.';
      });
      this.messageService.add({severity: 'error', summary: 'Thông Báo', detail: errorMessage, life: 10000});
    }
  }

  resetAddAddressForm() {
    if (this._formValue !== null) {
      this.form.reset(this._formValue);
    } else {
      this.form.reset();
    }
  }

  openDialog(address?: IAddress) {
    this.display = true;
    if (address) {
      this.addressId = address._id;
      const formValue = {
        name: address.name,
        phone: address.phone,
        address: address.address
      };
      this.addressText = address.addressText;
      this.form.patchValue(formValue);
      this._formValue = {...this.form.value};
    } else {
      this.form.reset();
      this.addressId = null;
      this._formValue = null;
      this.addressText = 'Ho Chi Minh';
    }
  }

  onDialogAddressShowed() {
    this.addressDialog.onWindowResize();
  }

  private initForm() {
    this.form = this.fb.group({
      name: ['', [
        this.validatorService.checkRequired()
      ]],
      phone: ['', [
        this.validatorService.checkRequired(),
        this.validatorService.checkMinLength(10)
      ]],
      address: ['', [
        this.validatorService.checkRequired()
      ]],
      longitude: [null, [this.validatorService.checkRequired()]],
      latitude: [null, [this.validatorService.checkRequired()]]
    });


  }

  private fetchData() {
    const sub = this.profileService.getAddressList()
        .subscribe((response: IResAddressList) => {
          this.listAddress = response.data.entries;
        });
    this.subscriptions.push(sub);
  }
}
