import { Component, OnInit } from '@angular/core';
import { BasePage } from '../shared/components/base/base-page';
import { GlobalConstant } from '../shared/constants/global.constant';
import { AuthService, ILoggedInUser } from '../shared/services/auth.service';
import { SeoService } from '../shared/services/seo.service';
import UserType = GlobalConstant.UserType;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends BasePage implements OnInit {
  userInfo: ILoggedInUser = null;

  constructor(private _authService: AuthService,
              private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    const sub = this._authService.getUserInfo()
      .subscribe((userInfo: ILoggedInUser | null) => {
        if (userInfo) {
          this.userInfo = userInfo;
        }
      });
    this.subscriptions.push(sub);
    this.seoService.setHeader('Trang cá nhân');
  }
}

