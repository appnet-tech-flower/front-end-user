import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { BasePage } from '../../shared/components/base/base-page';

@Component({
  selector: 'app-demo-form',
  templateUrl: './demo-form.component.html',
  styleUrls: ['./demo-form.component.scss'],
  providers: [MessageService]
})
export class DemoFormComponent extends BasePage implements OnInit {

  form: FormGroup;

  submitted: boolean;

  genders: SelectItem[];

  constructor(private fb: FormBuilder) {
    super();
  }

  get diagnostic() {
    return JSON.stringify(this.form.value);
  }

  ngOnInit() {
    this.form = this.fb.group({
      firstname: ['', this.validatorService.checkRequired()],
      lastname: ['', this.validatorService.checkRequired()],
      password: ['', Validators.compose([
        this.validatorService.checkRequired(),
        this.validatorService.checkMinLength(6)
      ])],
      description: [''],
      gender: ['', this.validatorService.checkRequired()],
      images: [[
        {
          link: 'http://157.230.248.161:3100/images/temps/demo/2019/04/23/20190423100133_10begjb5jutmdlti',
          isDemo: false
        }
      ]]
    });

    this.genders = [];
    this.genders.push({label: 'Select Gender', value: ''});
    this.genders.push({label: 'Male', value: 'Male'});
    this.genders.push({label: 'Female', value: 'Female'});
  }

  onSubmit(value: string) {
    const countError = this.getNumberOfFormErrors(this.form);

    if (countError === 0) {
      this.submitted = true;
      this.messageService.add({severity: 'info', summary: 'Success', detail: 'Form Submitted'});
    }
  }

  onChangeLink(value: string[]) {
    console.log('link image upload', value);
  }

}
