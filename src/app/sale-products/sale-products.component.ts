import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IResSearchBoxResult, IResSearchContent, SearchService } from '../shared/services/search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { SeoService } from '../shared/services/seo.service';
import { Paginator } from 'primeng/primeng';
import { BasePage } from '../shared/components/base/base-page';
import { IResListProducts, SaleProductsService } from './sale-products.service';

const PRICE_OPTIONS = [
  {label: 'Không chọn', value: {sb: '', sd: ''}},
  {label: 'Từ thấp đến cao', value: {sb: 'originalPrice', sd: 'ASC'}},
  {label: 'Từ cao đến thấp', value: {sb: 'originalPrice', sd: 'DESC'}}
];

@Component({
  selector: 'app-sale-products',
  templateUrl: './sale-products.component.html',
  styleUrls: ['./sale-products.component.scss']
})
export class SaleProductsComponent extends BasePage implements OnInit {

  searchQuery: any;
  priceOptions: Array<any> = PRICE_OPTIONS;
  products: any[] = [];
  conf: any = {
    page: 1,
    limit: 12,
    sb: '',
    sd: '',
    totalItems: 100
  };
  title = 'SẢN PHẨM KHUYẾN MÃI';
  searchFeature: any;

  @ViewChild('paginator') paginator: Paginator;

  constructor(private searchService: SearchService,
              private saleProductsService: SaleProductsService,
              private activatedRoute: ActivatedRoute,
              private seoService: SeoService,
              private router: Router) {
    super();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.globalService.setBreadcrumbs([]);
    this.fetchData();
  }

  changePage(event: any) {
    this.conf.page = event.page + 1;
    this.fetchData();
  }

  onChangeSort(event: any) {
    Object.assign(this.conf, event.value.value);
    this.fetchData();
    this.setToFirstPage();
  }

  setToFirstPage() {
    if (this.paginator) {
      this.paginator.changePage(0);
      (this.paginator as any).cd.detectChanges();
    }
  }

  private fetchData() {
    const params = this.generateParams();
    const sub = this.saleProductsService.listSaleProducts(params)
        .subscribe((res: IResListProducts) => {
          if (res.status === HTTP_CODES.SUCCESS) {
            console.log(res);
            this.products = res.data.products;
            this.conf.totalItems = res.data.meta.totalItems;
            this.searchQuery = res.data.searchQuery;
          }
        });

    this.subscriptions.push(sub);
  }

  private generateParams(): any {
    const params = {
      limit: this.conf.limit,
      page: this.conf.page,
      sb: this.conf.sb,
      sd: this.conf.sd
    };
    if (params.sb === '') {
      delete params.sb;
    }

    if (params.sd === '') {
      delete params.sd;
    }

    return params;
  }

  onSearch(searchBoxParams: any) {
    const sub = this.searchService.searchBox(searchBoxParams)
        .subscribe((res: IResSearchBoxResult) => {
          if (res.status === HTTP_CODES.SUCCESS) {
            this.router.navigate([`danh-muc/${res.data.url}`]);
          }
        });

    this.subscriptions.push(sub);
  }

}
