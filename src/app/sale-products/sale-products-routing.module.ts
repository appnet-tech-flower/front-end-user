import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SaleProductsComponent } from './sale-products.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      pathMatch: 'full',
      component: SaleProductsComponent
    }
  ])],
  exports: [RouterModule]
})
export class SaleProductsRoutingModule {

}
