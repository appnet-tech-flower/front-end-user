import { NgModule } from '@angular/core';
import { OrderLookupComponent } from './order-lookup.component';
import { CardModule } from 'primeng/card';
import { ButtonModule, InputTextModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    OrderLookupComponent
  ],
  imports: [
    CardModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    CommonModule
  ],
  providers: [],
  exports: []
})

export class OrderLookupModule {}
