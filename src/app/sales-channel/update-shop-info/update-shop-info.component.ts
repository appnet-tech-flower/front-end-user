import { AfterContentInit, Component, OnInit } from '@angular/core';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { SearchSelector } from '../../shared/constants/search-selector.constant';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProfileService } from '../../profile/profile.service';
import { SeoService } from '../../shared/services/seo.service';
import { BasePage } from '../../shared/components/base/base-page';
import { IMap } from '../../shared/components/map/map.component';

export interface IBody {
  availableShipCountry: boolean;
  availableShipAddresses?: {
    city: string,
    district?: number
  }[];
  address: string;
  longitude?: number;
  latitude?: number;
}

export interface IResShop {
  status: number;
  messages: string[];
  data: {};
}

interface ILocation {
  district: any;
  selectedCityCode: string;
  selectedDistrict: number[];
  isValid: boolean;
  isDuplicateCity: boolean;
  isSelectAllDistrict: boolean;
}

interface ICollectionLocation {
  amountOfError: number;
  locations: ILocation[];
}

@Component({
  selector: 'app-update-shop-info',
  templateUrl: './update-shop-info.component.html',
  styleUrls: ['./update-shop-info.component.scss']
})
export class UpdateShopInfoComponent extends BasePage implements OnInit, AfterContentInit {

  form: FormGroup;
  deliveryFlag = false;
  collectionLocation: ICollectionLocation = {amountOfError: 0, locations: []};
  citySource = SearchSelector.Cities;
  _location = {
    latitude: 10.8230989,
    longitude: 106.6296638
  };

  constructor(private fb: FormBuilder,
              private profileService: ProfileService,
              private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    this.initForm();
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Trang cá nhân',
        routerLink: '/trang-ca-nhan',
        icon: 'pi pi-user'
      },
      {
        label: 'Đăng ký bán hàng',
        routerLink: '/trang-ca-nhan/dang-ky-ban-hang',
        icon: 'pi pi-th-large'
      }
    ]);
    this.initLocation();
    this.seoService.setHeader('Trang cá nhân - Đăng ký bán hàng');
  }

  ngAfterContentInit() {
    this.getDetailShop();
  }

  onChangeCity(option: any, index: number) {
    const isDuplicateCity = this.collectionLocation.locations
        .filter(city => city.selectedCityCode === option.value.code).length;
    if (isDuplicateCity > 0) {
      this.collectionLocation.locations[index].isDuplicateCity = false;
      this.collectionLocation.locations[index].selectedCityCode = null;
      this.collectionLocation.locations[index].selectedDistrict = [];
      this.collectionLocation.locations[index].district = null;
    } else {
      this.collectionLocation.locations[index].isDuplicateCity = true;
      this.collectionLocation.locations[index].isValid = true;
      this.collectionLocation.locations[index].isSelectAllDistrict = true;
      this.collectionLocation.locations[index].selectedCityCode = option.value.code;
      this.collectionLocation.locations[index].selectedDistrict = option.value.districts.map(d => d.id);
      this.collectionLocation.locations[index].district = option.value.districts;
    }
  }

  onChangeSwitch(event: any) {
    this.deliveryFlag = event.checked;
  }

  onMapChanged(value: IMap | null) {
    if (value) {
      this._location = {
        longitude: value.longitude,
        latitude: value.latitude
      };
      this.form.controls.address.setValue(value.address);
      this.form.controls.longitude.setValue(value.longitude);
      this.form.controls.latitude.setValue(value.latitude);
    }
  }

  onSubmit() {
    const countError = this.getNumberOfFormErrors(this.form);
    this.validateCollectionLocation();
    /*    if (countError === 0 && this.collectionLocation.amountOfError === 0) {
          const body = this.exchangeFormToBody();
          this.updateShop(body);
        }*/
    const body = this.exchangeFormToBody();
    this.updateShop(body);
  }

  updateShop(body: IBody) {
    const sub = this.profileService.updateShop(body)
        .subscribe((response: IResShop) => {
          if (response.status === HTTP_CODES.SUCCESS) {
            this.messageService.add({
              severity: 'success',
              summary: 'Thông Báo',
              detail: 'Cập nhật thông tin thành công',
              life: 10000
            });
          } else {
            let errorMessage = '';
            response.messages.forEach(event => {
              errorMessage += event + '.';
            });
            this.messageService.add({severity: 'error', summary: 'Thông Báo', detail: errorMessage, life: 10000});
          }
        });
    this.subscriptions.push(sub);
  }

  exchangeFormToBody(): IBody {
    const body = {
      availableShipCountry: this.deliveryFlag,
      availableShipAddresses: [],
      address: this.form.value.address,
      longitude: this.form.value.longitude,
      latitude: this.form.value.latitude
    };
    if (!this.deliveryFlag) {
      this.collectionLocation.locations.forEach(city => {
        city.selectedDistrict.forEach(districtId => {
          body.availableShipAddresses.push({city: city.selectedCityCode, district: districtId});
        });
      });
      return body;
    }
    return body;
  }

  initLocation() {
    this.addLocation();
  }

  addLocation() {
    const location = {
      district: null,
      selectedCityCode: null,
      selectedDistrict: [],
      isValid: true,
      isDuplicateCity: true,
      isSelectAllDistrict: true
    };
    this.collectionLocation.locations.push(location);
  }

  removeLocation(index: number) {
    this.collectionLocation.locations.splice(index, 1);
  }

  chooseDistricts(index: number) {
    const existDistrict = this.collectionLocation.locations[index].selectedDistrict.length;
    const amountOfDistricts = this.collectionLocation.locations[index].district.length;
    this.collectionLocation.locations[index].isSelectAllDistrict = existDistrict >= amountOfDistricts;
  }

  selectAllDistrictsOfCity(event: any, locationIndex: number) {
    if (event) {
      this.collectionLocation.locations[locationIndex].selectedDistrict = this.collectionLocation.locations[locationIndex].district
          .map(d => d.id);
      this.collectionLocation.locations[locationIndex].isSelectAllDistrict = true;
    } else {
      this.collectionLocation.locations[locationIndex].selectedDistrict = [];
      this.collectionLocation.locations[locationIndex].isSelectAllDistrict = false;
    }
  }

  private initForm() {
    this.form = this.fb.group({
      address: [[], [
        this.validatorService.checkRequired()
      ]],
      longitude: [null, [this.validatorService.checkRequired()]],
      latitude: [null, [this.validatorService.checkRequired()]]
    });
  }

  private getDetailShop() {
    const sub = this.profileService.getDetailShop()
        .subscribe((response: any) => {
          if (response.status === HTTP_CODES.SUCCESS) {
            this.mappingData(response.data);
          }
        });
    this.subscriptions.push(sub);
  }

  private mappingData(data) {
    this.onMapChanged(data.address);
    this._location = {
      longitude: data.address.longitude,
      latitude: data.address.latitude
    };
    const addressControl = this.form.get('address');
    addressControl.setValue(data.address.address);
    addressControl.updateValueAndValidity();
  }

  private validateCollectionLocation() {
    if (!this.deliveryFlag) {
      this.collectionLocation.amountOfError = this.collectionLocation.locations
          .map(element => {
            if (element.selectedCityCode === null || element.selectedDistrict.length === 0) {
              element.isValid = false;
            }
            return element.selectedCityCode;
          })
          .filter((cityStr: string) => cityStr === null)
          .length;
    } else {
      this.collectionLocation.amountOfError = 0;
    }
  }
}
