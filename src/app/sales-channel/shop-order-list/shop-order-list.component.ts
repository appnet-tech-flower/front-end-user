import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { SalesChannelService } from '../sales-channel.service';
import { IEventOrderItemStatus } from './shop-order-item/shop-order-item.component';
import { ConfirmationService } from 'primeng/api';

export interface IResShopOrderList {
  status: number;
  messages: string[];
  data: {
    meta: {
      totalItems: number;
    };
    orders: IShopOrder[];
  };
}

export interface IResUpdateOrderStatus {
  status: number;
  messages: string[];
  data: {};
}

export interface IShopOrderItem {
  _id: string;
  quantity: number;
  price: number;
  product: {
    _id: string;
    originalPrice: number;
    title: string;
    slug: string;
    images: string[];
    saleOff: {
      price: number;
      startDate: Date;
      endDate: Date;
      active: boolean
    }
  };
  shippingCost: number;
  createdAt: Date;
}

export interface IShopOrder {
  _id: string;
  count: number;
  total: number;
  orderItems: IShopOrderItem[];
  order: {
    status: number;
    buyerInfo: any;
    code: string;
    deliveryTime: Date;
    paidAt: Date;
    submitAt: Date;
  };
  user: {
    email: string;
    name: string;
    phone: string;
  };
  address: {
    name: string;
    phone: string;
    address: string;
    addressText: string;
    latitude: number;
    longitude: number;
  };
}

export interface IQueryParam {
  limit?: number;
  page?: number;
  status?: number;
  sb?: string;
  sd?: string;
  startDate?: string;
  endDate?: string;
}

export enum Tab {
  ALL,
  BEING_TRANSPORTED,
  SUCCESS,
  NEW
}

@Component({
  selector: 'app-shop-order-list',
  templateUrl: './shop-order-list.component.html',
  styleUrls: ['./shop-order-list.component.scss'],
  providers: [ConfirmationService]
})

export class ShopOrderListComponent extends BasePage implements OnInit {
  queryParam: IQueryParam = {limit: 10, page: 1};
  orderList: IShopOrder[];
  totalItems: number;
  tabView: number;
  status = GlobalConstant.Status;
  isFirstPage = false;
  searchCondition = {
    startDate: null,
    endDate: null
  };

  constructor(private confirmationService: ConfirmationService, private salesChannelService: SalesChannelService) {
    super();
  }

  ngOnInit(): void {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Kênh người bán',
        routerLink: '/kenh-nguoi-ban',
        icon: 'pi pi-user',
      },
      {
        label: 'Danh sách đơn đặt hàng',
        routerLink: '/kenh-nguoi-ban/don-dat-hang',
        icon: 'pi pi-th-large'
      }
    ]);
    this.fetchData();
  }

  fetchData() {
    this.globalService.setLoadingSpinner(true);
    const sub = this.salesChannelService.getShopOrderList(this.queryParam)
      .subscribe((response: IResShopOrderList) => {
        this.orderList = response.data.orders;
        this.totalItems = response.data.meta.totalItems;
        this.globalService.setLoadingSpinner(false);
      });

    this.subscriptions.push(sub);
  }

  onPageChanged(page: number) {
    this.queryParam.page = page;
    this.fetchData();
  }

  onChangeTabView(event: any) {
    this.tabView = event.index;
    this.queryParam.page = 1;
    this.isFirstPage = true;
    this.orderList = [];

    if (Tab.BEING_TRANSPORTED === this.tabView) {
      this.queryParam.status = this.status.ORDER_ITEM_ON_DELIVERY;
    } else if (Tab.SUCCESS === this.tabView) {
      this.queryParam.status = this.status.ORDER_ITEM_FINISHED;
    } else if (Tab.NEW === this.tabView) {
      this.queryParam.status = this.status.ORDER_ITEM_PROCESSING;
    } else {
      delete this.queryParam.status;
    }
    this.fetchData();
  }

  onStatusChange(event: IEventOrderItemStatus) {
    this.confirmationService.confirm({
      message: 'Bạn có chắc muốn thay đổi trạng thái của đơn hàng này?',
      accept: () => {
        const sub = this.salesChannelService.updateOrderStatus(event)
          .subscribe((response: IResShopOrderList) => {
            if (response.status === HTTP_CODES.SUCCESS) {
              this.isFirstPage = true;
              this.fetchData();
            }
          });
        this.subscriptions.push(sub);
      }
    });
  }

  onSearch() {
    Object.keys(this.searchCondition).forEach(key => {
      if (this.searchCondition[key] !== null) {
        this.queryParam[key] = this.searchCondition[key];
      } else {
        delete this.queryParam[key];
      }
    });
    this.fetchData();
  }

  onReset() {
    Object.keys(this.searchCondition).forEach(key => {
      this.searchCondition[key] = null;
      delete this.queryParam[key];
    });
  }
}
