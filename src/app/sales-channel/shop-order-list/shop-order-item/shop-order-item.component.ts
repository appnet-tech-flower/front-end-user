import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { Paginator } from 'primeng/primeng';
import { environment } from '../../../../environments/environment';
import { BasePage } from '../../../shared/components/base/base-page';
import { GlobalConstant } from '../../../shared/constants/global.constant';
import { IShopOrder } from '../shop-order-list.component';

export interface IEventOrderItemStatus {
  status: number;
  orderItemIds: string[];
}

@Component({
  selector: 'app-shop-order-item',
  templateUrl: './shop-order-item.component.html',
  styleUrls: ['./shop-order-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ShopOrderItemComponent extends BasePage implements OnInit {
  @Input() orderList: IShopOrder[];
  @Input() totalItems: number;
  @Output() pageChange = new EventEmitter<number>();
  @Output() statusState = new EventEmitter<IEventOrderItemStatus>();
  @ViewChild('paginator') paginator: Paginator;
  status = GlobalConstant.Status;
  environment = environment;
  sizeImage = GlobalConstant.SizeImage;

  ngOnInit(): void {
  }

  onPageChange(event: any) {
    this.pageChange.emit(event.page + 1);
  }

  @Input()
  set setToFirstPage(isFirstPage: boolean) {
    if (this.paginator && isFirstPage) {
      this.paginator.changePage(0);
      (this.paginator as any).cd.detectChanges();
    }
  }

  getStatus(status: number, index: number) {
    const params: IEventOrderItemStatus = {
      orderItemIds: this.orderList[index].orderItems.map(oi => oi._id),
      status
    };

    this.statusState.emit(params);
  }
}
