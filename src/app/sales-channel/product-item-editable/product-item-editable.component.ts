import { Component, EventEmitter, Input, Output } from '@angular/core';
import { environment } from '../../../environments/environment';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { IProduct } from '../list-product/list-product.component';
import SizeImage = GlobalConstant.SizeImage;

@Component({
  selector: 'app-shop-product-item-editable',
  templateUrl: './product-item-editable.component.html',
  styleUrls: ['./product-item-editable.component.scss']
})

export class ShopProductItemEditableComponent {
  private _product: IProduct = null;
  status = GlobalConstant.Status;

  SizeImage = SizeImage;

  @Input()
  set product(value: IProduct) {
    this.mapImage(value);
    this._product = value;
  }

  get product(): IProduct {
    return this._product;
  }

  @Input() value: boolean;

  @Input() disabledCheckbox = false;

  @Output() valueChange = new EventEmitter();
  @Output() changeSelection = new EventEmitter();

  onChangeCheckBox(value: boolean) {
    this.valueChange.emit(value);
    this.changeSelection.emit();
  }

  mapImage(product: IProduct) {
    product.thumbnail = product.images[0];
  }
}
