import { NgModule } from '@angular/core';
import { SalesChannelComponent } from './sales-channel.component';
import { CardModule } from 'primeng/card';
import { RouterModule } from '@angular/router';
import { SalesChannelRoutingModule } from './sales-channel-routing.module';
import { SalesChannelService } from './sales-channel.service';
import {
  ButtonModule, CalendarModule,
  CheckboxModule, ChipsModule,
  ConfirmDialogModule,
  DropdownModule, EditorModule, InputSwitchModule,
  InputTextModule, MessageModule,
  PaginatorModule, PanelModule, SplitButtonModule,
  TabViewModule, ToolbarModule
} from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListProductComponent } from './list-product/list-product.component';
import { CommonModule, DecimalPipe } from '@angular/common';
import { ShopProductItemEditableComponent } from './product-item-editable/product-item-editable.component';
import { NewProductComponent } from './new-product/new-product.component';
import { ImageUploaderModule } from '../shared/components/image-uploader/imageUploader.module';
import { ToastModule } from 'primeng/toast';
import { ShopOrderListComponent } from './shop-order-list/shop-order-list.component';
import { ShopOrderItemComponent } from './shop-order-list/shop-order-item/shop-order-item.component';
import { ShopStatisticComponent } from './shop-statistic/shop-statistic.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from '../shared/services/token-interceptor.service';
import { UpdateShopInfoComponent } from './update-shop-info/update-shop-info.component';
import { MapModule } from '../shared/components/map/map.module';
import { AppStaticImageModule } from '../shared/pipes/app-static-image/app-static-image.module';
import {TableModule} from 'primeng/table';
import { ShopProductItemTableComponent } from './product-item-table/product-item-table.component';
@NgModule({
  declarations: [
    SalesChannelComponent,
    ListProductComponent,
    ShopProductItemEditableComponent,
    NewProductComponent,
    ShopOrderListComponent,
    ShopOrderItemComponent,
    ShopStatisticComponent,
    UpdateShopInfoComponent,
    ShopProductItemTableComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    RouterModule,
    SalesChannelRoutingModule,
    CheckboxModule,
    FormsModule,
    DropdownModule,
    ButtonModule,
    InputSwitchModule,
    TabViewModule,
    PaginatorModule,
    ConfirmDialogModule,
    InputTextModule,
    ReactiveFormsModule,
    MessageModule,
    CalendarModule,
    EditorModule,
    ChipsModule,
    ImageUploaderModule,
    ToastModule,
    MapModule,
    PanelModule,
    AppStaticImageModule,
    TableModule
  ],
  providers: [
    SalesChannelService,
    DecimalPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
  exports: [RouterModule]
})

export class SalesChannelModule {
}
