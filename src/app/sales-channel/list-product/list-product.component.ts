import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, Paginator } from 'primeng/primeng';
import { BasePage } from '../../shared/components/base/base-page';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { SeoService } from '../../shared/services/seo.service';
import { IResUpdateProductsStatus } from '../../profile/profile.service';
import { ListProduct } from './list-product.constant';
import { SalesChannelService } from '../sales-channel.service';

export interface IProduct {
  _id: string;
  title: string;
  images: string[];
  thumbnail?: string; // just use in FE
  sku: string;
  description: string;
  topic: number;
  specialOccasion?: number;
  design?: number;
  floret?: number;
  city?: string;
  district?: number;
  color?: number;
  priceRange: number;
  slug: string;
  seoUrl?: string;
  seoDescription?: string;
  seoImage?: string;
  originalPrice: number;
  code: string;
  updatedAt: Date;
  createdAt: Date;
  user: string;
  status: number;
  view: number;
  sold?: number;
  addToCart: number;
  saleOff: {
    price: number;
    startDate: Date;
    endDate: Date;
    active: boolean;
  };
}

export interface IResProductList {
  status: number;
  messages: string[];
  data: {
    meta: MetaProduct,
    products: IProduct[]
  };
}

export interface IQueryParam {
  limit?: number;
  page?: number;
  title?: string;
  status?: number;
  approvedStatus?: number;
  sb?: string;
  sd?: string;
}

export interface MetaProduct {
  totalItems: number;
}

enum TabNms {
  All,
  Hidden,
  Pending,
  Not_Approved
}

@Component({
  selector: 'app-profile-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss'],
  providers: [ConfirmationService]
})

export class ListProductComponent extends BasePage implements OnInit {
  searchSelector = ListProduct;
  listProduct: IProduct[] = [];
  metaProduct: MetaProduct = {
    totalItems: 0
  };
  queryParams: IQueryParam = {
    approvedStatus: GlobalConstant.Status.PRODUCT_APPROVED,
    status: GlobalConstant.Status.ACTIVE,
    limit: 11
  };
  textBoxParam: string;
  listProductCheckObj: any = {};
  selectAllProduct = false;
  numberOfSelected = 0;
  tabIndexToEnum = {
    0: TabNms.All,
    1: TabNms.Hidden,
    2: TabNms.Pending,
    3: TabNms.Not_Approved
  };

  isTable = false;

  @ViewChild('paginator') paginator: Paginator;

  constructor(private confirmationService: ConfirmationService,
              private router: Router,
              private seoService: SeoService,
              private salesChannelService: SalesChannelService) {
    super();
  }
  changeIsTableState(state) {
    this.isTable = state;
    this.globalService.setLoadingSpinner(true);
    setTimeout(() => {
      this.globalService.setLoadingSpinner(false);
    }, 500);
  }

  ngOnInit(): void {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Kênh người bán',
        routerLink: '/kenh-nguoi-ban',
        icon: 'pi pi-user',
      },
      {
        label: 'Danh sách sản phẩm',
        routerLink: '/kenh-nguoi-ban/danh-sach-san-pham',
        icon: 'pi pi-th-large'
      }
    ]);

    this.fetchData();
    this.seoService.setHeader('Kênh người bán - Danh sách sản phẩm');
  }

  onChangeSort(option: any) {
    Object.assign(this.queryParams, option.value.value);
    this.setToFirstPage();
  }

  search(value: string) {
    this.queryParams.title = value;
    this.setToFirstPage();
  }

  onPageChange(event: any) {
    this.queryParams.page = event.page + 1;
    this.fetchData();
  }

  confirmActiveProducts() {
    if (this.numberOfSelected === 0) {
      return;
    }

    this.confirmationService.confirm({
      message: 'Bạn có chắc muốn HIỆN những sản phẩm này',
      accept: () => {
        this.changeStatusProduct(GlobalConstant.Status.ACTIVE);
      }
    });
  }

  confirmHideProducts() {
    if (this.numberOfSelected === 0) {
      return;
    }

    this.confirmationService.confirm({
      message: 'Bạn có chắc muốn Ẩn những sản phẩm này',
      accept: () => {
        this.changeStatusProduct(GlobalConstant.Status.PRODUCT_HIDDEN);
      }
    });
  }

  confirmDeleteProducts() {
    if (this.numberOfSelected === 0) {
      return;
    }

    this.confirmationService.confirm({
      message: 'Bạn có chắc muốn XÓA những sản phẩm này',
      accept: () => {
        this.changeStatusProduct(GlobalConstant.Status.DELETE);
      }
    });
  }

  handleChange(event) {
    const tabNm = this.tabIndexToEnum[event.index];
    switch (tabNm) {
      case TabNms.All:
        this.queryParams.status = GlobalConstant.Status.ACTIVE;
        this.queryParams.approvedStatus = GlobalConstant.Status.PRODUCT_APPROVED;
        break;
      case TabNms.Hidden:
        this.queryParams.status = GlobalConstant.Status.PRODUCT_HIDDEN;
        this.queryParams.approvedStatus = GlobalConstant.Status.PRODUCT_APPROVED;
        break;
      case TabNms.Pending:
        delete this.queryParams.status;
        this.queryParams.approvedStatus = GlobalConstant.Status.PRODUCT_PENDING_APPROVE;
        break;
      case TabNms.Not_Approved:
        delete this.queryParams.status;
        this.queryParams.approvedStatus = GlobalConstant.Status.PRODUCT_NOT_APPROVED;
    }

    this.setToFirstPage();
  }

  private fetchData() {
    this.globalService.setLoadingSpinner(true);
    const sub = this.salesChannelService.getListProduct(this.queryParams)
      .subscribe((response: IResProductList) => {
        if (response.status === HTTP_CODES.SUCCESS) {
          this.listProduct = response.data.products;
          this.metaProduct = response.data.meta;

          this.selectAllProduct = false;
          this.toggleSelectAll(this.selectAllProduct);
          this.countSelection();
        }

        setTimeout(() => {
          this.globalService.setLoadingSpinner(false);
        }, 100);
      });
    this.subscriptions.push(sub);
  }

  private changeStatusProduct(status: number) {
    const productIds = this.getSelectedProductIds();
    this.salesChannelService.updateProductsStatus(productIds, status)
      .subscribe((res: IResUpdateProductsStatus) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.setToFirstPage();
        }

        // TODO: use messageService to show error messages
      });
  }

  private getSelectedProductIds(): string[] {
    return Object.keys(this.listProductCheckObj).filter(k => this.listProductCheckObj[k] === true);
  }

  onChangeCheckBox() {
    this.countSelection();
  }

  countSelection() {
    this.numberOfSelected = Object.values(this.listProductCheckObj)
      .filter(v => v === true)
      .length;

    if (this.numberOfSelected === this.listProduct.length && this.listProduct.length !== 0) {
      this.selectAllProduct = true;
    } else {
      this.selectAllProduct = false;
    }
  }

  setToFirstPage() {
    this.paginator.changePage(0);
    (this.paginator as any).cd.detectChanges();
  }

  checkAll() {
    this.toggleSelectAll(this.selectAllProduct);
    this.countSelection();
  }

  toggleSelectAll(value: boolean) {
    this.listProduct.forEach((p: IProduct) => {
      this.listProductCheckObj[p._id] = value;
    });
  }


  navigateToCreateProductPage() {
    this.router.navigate(['/kenh-nguoi-ban/dang-san-pham']);
  }
}
