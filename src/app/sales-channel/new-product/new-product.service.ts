import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API } from '../../shared/constants/api.constant';
@Injectable({
  providedIn: 'root'
})
export class NewProductService {

  constructor(private _http: HttpClient) { }
  createProduct(data: any): Observable<any> {
    return this._http.post(API.Product.NewProduct, data);
  }

  updateProduct(id: string, data: any): Observable<any> {
    const url = API.Product.UpdateProduct.replace('{id}', id);
    return this._http.put(url, data);
  }
  getDetail(id: string): Observable<any> {
    const url = API.Product.DetailProduct.replace('{id}', id);
    return this._http.get(url);
  }
}
