import { AfterContentInit, Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { SearchSelector } from '../../shared/constants/search-selector.constant';
import { EditableFormBaseComponent } from '../../shared/components/base/editable-form-base.component';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NewProductService } from './new-product.service';
import { StringService } from '../../shared/services/string.service';
import Status = GlobalConstant.Status;
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})

export class NewProductComponent extends EditableFormBaseComponent implements OnInit, AfterContentInit {
  searchSelector = SearchSelector;
  showSelectSpecialOccasion = false;
  form: FormGroup;
  selectedStatus = Status.ACTIVE;
  onFlowEdit = false;
  isGiftOrComoTopic = false;
  params: any = {};
  disableSettings = {
    sale: true
  };
  oldData: any = null;
  private _initFormValue: any = {};

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private newProductService: NewProductService,
    private decimalPipe: DecimalPipe) {
    super();

    const id = this.activatedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.params.id = id;
      this.onFlowEdit = true;
    }
  }

  ngAfterContentInit() {
    this.scrollTop();
    this.loadProductDetail();
  }

  post() {
    const params = this.generatePostObject();
    let req: Observable<any>;

    if (this.onFlowEdit) {
      req = this.newProductService.updateProduct(this.params.id, params);
    } else {
      req = this.newProductService.createProduct(params);
    }

    this.globalService.setLoadingSpinner(true);
    const sub = req
      .subscribe((res: any) => {
        this.globalService.setLoadingSpinner(false);
        const message = res.messages.join('\n');
        if (res.status !== 200) {
          this.messageService.add({ severity: 'error', summary: 'Có lỗi', detail: message });
          return;
        }

        this.messageService.add({ severity: 'info', summary: 'Thông báo', detail: message });
        this.scrollTop();
        this.router.navigate(['/kenh-nguoi-ban/danh-sach-san-pham']);
      });
    this.subscriptions.push(sub);
  }

  onClickBtnCancel() {
    this.form.reset(this._initFormValue);
  }

  onClickBtnSaveAndHide() {
    this.selectedStatus = Status.PRODUCT_HIDDEN;
    this.onSubmit();
  }

  onClickBtnSaveAndShow() {
    this.selectedStatus = Status.ACTIVE;
    this.onSubmit();
  }

  onTopicChanged(event: any) {
    if (event.value) {
      const value = event.value.value;
      this.showSelectSpecialOccasion = value === 9;

      const floretControl = this.form.get('floret');
      const skuControl = this.form.get('sku');
      if (value === SearchSelector.TopicsForShop[11].value || value === SearchSelector.TopicsForShop[12].value || value === SearchSelector.TopicsForShop[13].value) {
        this.isGiftOrComoTopic = true;
        floretControl.clearValidators();
        floretControl.updateValueAndValidity();
        skuControl.clearValidators();
        skuControl.updateValueAndValidity();
      } else {
        this.isGiftOrComoTopic = false;
        floretControl.setValidators([Validators.required]);
        floretControl.updateValueAndValidity();
        skuControl.clearValidators();
        skuControl.updateValueAndValidity();
      }
    }
  }

  ngOnInit(): void {
    this._initBreadcrumbs();
    this._initForm();
  }

  onChangedSaleActive(e: any) {
    let flag = e;
    if (e.target) {
      flag = e.target.checked;
    }
    const salePriceControl = this.form.get('salePrice');
    const startDateControl = this.form.get('startDate');
    const endDateControl = this.form.get('endDate');
    if (flag) {
      if (!startDateControl.value && !this.onFlowEdit) {
        startDateControl.setValue(new Date());
      }
      if (!endDateControl.value && !this.onFlowEdit) {
        endDateControl.setValue(new Date(new Date().setMonth(new Date().getMonth() + 1)));
      }
      this.disableSettings.sale = false;
      salePriceControl.setValidators([Validators.required]);
      startDateControl.setValidators([Validators.required]);
      endDateControl.setValidators([Validators.required]);
    } else {
      this.disableSettings.sale = true;
      salePriceControl.clearValidators();
      startDateControl.clearValidators();
      endDateControl.clearValidators();
    }
    startDateControl.updateValueAndValidity();
    endDateControl.updateValueAndValidity();
    salePriceControl.updateValueAndValidity();
  }

  loadProductDetail(): void {
    if (this.params.id) {
      const sub = this.newProductService.getDetail(this.params.id)
        .subscribe(res => {
          const message = res.messages.join('\n');
          if (res.status !== 200) {
            alert([message]);
            return;
          } else {
            this.oldData = res.data;
            this.setValueForm(res.data);
          }
        });
      this.subscriptions.push(sub);
    }

  }

  setValueForm(data: any) {
    const params = { ...data };

    params.images = StringService.mapIFileTextValue(params.images);
    params.salePrice = this.exchangeFormatNumber(params.saleOff.price);
    params.saleActive = params.saleOff.active;
    this.onChangedSaleActive(params.saleOff.active);
    params.freeShip = params.freeShip;
    const today = new Date();
    const oneMonthFromToday = new Date(new Date().setMonth(new Date().getMonth() + 1));
    const nullDate = (new Date(null));
    params.startDate = new Date(params.saleOff.startDate);
    params.endDate = new Date(params.saleOff.endDate);
    params.startDate = nullDate.getTime() !== params.startDate.getTime() ? params.startDate : today;
    params.endDate = nullDate.getTime() !== params.endDate.getTime() ? params.endDate : oneMonthFromToday;
    params.originalPrice = this.exchangeFormatNumber(params.originalPrice);
    params.keywordList = params.tags;
    params.color = StringService.getColorByValue(params.color);
    params.topic = StringService.getTopicsByValue(params.topic);
    params.design = StringService.getDesignByValue(params.design);
    params.floret = StringService.getFloretByValue(params.floret);

    if (params.topic) {
      this.onTopicChanged({ value: params.topic });
      if (params.specialOccasion) {
        params.specialOccasion = StringService.getSpecialOccasionByValue(params.specialOccasion);
      }
    }
    this.form.patchValue(params);
  }

  onChangeInput(event: any, position: boolean) {
    const numberSplitComma = event.target.value.replace(/,/g, '');
    if (position) {
      this.form.controls.originalPrice.setValue(this.decimalPipe.transform(numberSplitComma));
    } else {
      this.form.controls.salePrice.setValue(this.decimalPipe.transform(numberSplitComma));
    }
  }

  private generatePostObject(): any {
    const params = { ...this.form.value };
    if (!params.color) {
      delete params.color;
    } else {
      params.color = params.color.value;
    }

    if (!params.floret) {
      delete params.floret;
    } else {
      params.floret = params.floret.value;
    }

    if (!params.design) {
      delete params.design;
    } else {
      params.design = params.design.value;
    }

    if (!params.sku) {
      delete params.sku;
    } else {
      params.sku = params.sku;
    }

    if (!params.topic) {
      delete params.topic;
    } else {
      params.topic = params.topic.value;
    }

    if (!params.specialOccasion) {
      delete params.specialOccasion;
    } else {
      params.specialOccasion = params.specialOccasion.value;
    }
    if (!params.startDate) {
      delete params.startDate;
    }

    if (!params.endDate) {
      delete params.endDate;
    }

    if (params.salePrice) {
      params.salePrice = params.salePrice.replace(/,/g, '');
    } else {
      delete params.salePrice;
    }
    params.originalPrice = params.originalPrice.replace(/,/g, '');
    params.status = this.selectedStatus;
    params.images = params.images ? StringService.mapDataImage(params.images) : [];
    return params;
  }

  private _initBreadcrumbs() {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Kênh người bán',
        routerLink: '/kenh-nguoi-ban',
        icon: 'pi pi-user',
      },
      {
        label: 'Đăng sản phẩm',
        routerLink: '/kenh-nguoi-ban/dang-san-pham',
        icon: 'pi pi-th-large'
      }
    ]);
  }

  private _initForm() {
    this.form = this.fb.group({
      title: ['', [this.validatorService.checkRequired()]],
      description: ['', [this.validatorService.checkRequired()]],
      images: [[], [this.validatorService.checkRequired()]],
      topic: [null, [this.validatorService.checkRequired()]],
      floret: [null, [this.validatorService.checkRequired()]],
      specialOccasion: [null, []],
      design: [null, []],
      color: [null, []],
      originalPrice: ['', [this.validatorService.checkRequired()]],
      salePrice: [null, []],
      saleActive: [false],
      freeShip: [false],
      sku: ['', [this.validatorService.checkRequired()]],
      keywordList: [[]],
      startDate: [null],
      endDate: [null],
    });

    this._initFormValue = { ...this.form.value };
  }

  private exchangeFormatNumber(value: string): string {
    return this.decimalPipe.transform(value);
  }
}
