import { Component, OnInit } from '@angular/core';
import { BasePage } from '../shared/components/base/base-page';
import { SeoService } from '../shared/services/seo.service';

@Component({
  selector: 'app-sales-channel',
  templateUrl: './sales-channel.component.html',
  styleUrls: ['./sales-channel.component.scss']
})

export class SalesChannelComponent extends BasePage implements OnInit {
  constructor(private seoService: SeoService) {
    super();
  }
  ngOnInit(): void {
    this.seoService.setHeader('Kênh người bán');
  }
}
