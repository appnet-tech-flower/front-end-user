import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SearchService } from '../shared/services/search.service';
import { DetailPostResolverComponent } from './detail-post-resolver.component';
import { PostDirective } from './post.directive';


@NgModule({
  declarations: [
    DetailPostResolverComponent,
    PostDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    BrowserModule
  ],
  providers: [
    SearchService
  ],
  exports: [
    DetailPostResolverComponent
  ]
})

export class DetailPostResolverModule {

}
