import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturedProductsComponent } from './featured-products.component';
import { FeaturedProductsRoutingModule } from './featured-products-routing.module';
import { FeaturedProductsService } from './featured-products.service';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { CardModule, DropdownModule, PanelMenuModule, TabMenuModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';

@NgModule({
  declarations: [FeaturedProductsComponent],
  imports: [
    CommonModule,
    FeaturedProductsRoutingModule,
    CommonModule,
    TabMenuModule,
    PanelMenuModule,
    FormsModule,
    PaginatorModule,
    ProductItemModule,
    DropdownModule,
    SearchBoxModule,
    CardModule
  ],
  providers: [
      FeaturedProductsService
  ]
})
export class FeaturedProductsModule { }
