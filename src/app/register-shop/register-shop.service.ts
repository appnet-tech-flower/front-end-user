import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { IBody } from "../profile/address-list/address-list.component";
import { Observable } from "rxjs";
import { IResShop } from "../profile/register-shop/register-shop.component";
import { API } from "../shared/constants/api.constant";

@Injectable({
  providedIn: 'root'
})
export class RegisterShopService {

  constructor(private httpClient: HttpClient) {
  }

  registerShop(body: IBody): Observable<any> {
    return this.httpClient.post<any>(API.Profile.RegisterShop, body);
  }
}
