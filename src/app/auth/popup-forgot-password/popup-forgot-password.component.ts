import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { GlobalService } from '../../shared/services/global.service';
import { DynamicDialogRef } from 'primeng/api';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-popup-forgot-password',
  templateUrl: './popup-forgot-password.component.html',
  styleUrls: ['./popup-forgot-password.component.scss'],
})
export class PopupForgotPasswordComponent extends BasePage implements OnInit {

  forgotPasswordForm: FormGroup;

  constructor(public globalService: GlobalService,
              private _dialogRef: DynamicDialogRef,
              private _fb: FormBuilder) {
    super();
  }

  onHidePopup() {
    this._dialogRef.close();
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.forgotPasswordForm = this._fb.group({
      email: ['', [this.validatorService.checkRequired(), this.validatorService.checkEmail()]]
    });
  }

  onSubmit() {
    const email = Object.values(this.forgotPasswordForm.value).toString();
    this.globalService.setLoadingSpinner(true);
    const sub = this.authService.forgotPassword(email).subscribe(
      (res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess(res.messages[0]);
          this.globalService.setLoadingSpinner(false);
          this._dialogRef.close();
        } else {
          this.swalDialogService.openWarning(res.messages[0]);
          this.globalService.setLoadingSpinner(false);
        }
      }
    );
    this.subscriptions.push(sub);
  }
}
