import { AfterViewInit, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DynamicDialogRef } from 'primeng/api';
import { TabView } from 'primeng/primeng';
import { BasePage } from '../../shared/components/base/base-page';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { AuthService } from '../../shared/services/auth.service';
import { environment } from '../../../environments/environment';

declare var gapi: any;
declare var FB: any;

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent extends BasePage implements OnInit, AfterViewInit {
  iconInputResendOTP = 'pi pi-replay';
  showOTPPopup = false;
  showPhonePopup = false;
  auth2: any;
  registerPhone = '';
  otpInput = '';
  phoneInput = '';
  googleId = null;
  facebookId = null;
  @Output() showAuthPopupChange = new EventEmitter();
  @Output() changeTab = new EventEmitter();
  @ViewChild('tabView') tabView: TabView;
  @ViewChild('googleBtn') googleBtn;

  constructor(private _authService: AuthService) {
    super();
  }

  private _showAuthPopup: boolean;

  get showAuthPopup(): boolean {
    return this._showAuthPopup;
  }

  @Input()
  set showAuthPopup(value: boolean) {
    this._showAuthPopup = value;
  }

  ngAfterViewInit(): void {
    this.googleInit();
    this.facebookInit();
  }

  ngOnInit() {
    this.changeTab.emit();
  }

  onHidePopup() {
    this.showAuthPopupChange.emit(false);
  }

  onChangeTab() {
    this.changeTab.emit();
  }

  onRegisteringSuccessfully(phone: string) {
    if (phone !== '') {
      this.showOTPPopup = true;
      this.registerPhone = phone;
    }
  }

  loginByFacebook() {
    FB.login((response: any) => {
      console.log(response);
      this.globalService.setLoadingSpinner(true);

      if (!response.authResponse && !response.status) {
        this.globalService.setLoadingSpinner(false);
        return;
      }

      if (response.authResponse) {
        const token = response.authResponse.accessToken;
        const sub = this._authService.loginByFacebook(token).subscribe(
          (res: any) => {
            if (res.status === HTTP_CODES.SUCCESS) {
              this._authService.saveUserInto(res.data.entries[0]);
              this._authService.saveAccessToken(res.data.meta.token);
              window.location.reload();
            } else if (res.status === HTTP_CODES.CREATED) {
              this.swalDialogService.openSuccess(res.messages.join('.'));
              this.facebookId = res.data.facebookId;
              this.showPhonePopup = true;
            } else {
              this.swalDialogService.openError(res.messages.join('.'));
            }

            this.globalService.setLoadingSpinner(false);
          }
        );

        this.subscriptions.push(sub);
      } else {
        this.globalService.setLoadingSpinner(false);
        this.swalDialogService.openError('Không thể đăng nhập');
      }
    });
  }

  public attachSignIn(element: any) {
    this.auth2.attachClickHandler(
      element,
      {},
      (googleUser: any) => {
        this.onSignIn(googleUser);
      },
      (error: any) => {
        console.log('google error:' + error.error);
      });
  }

  onSignIn(googleUser: any) {
    const profile = googleUser.getBasicProfile();
    const googleId: number = profile.getId();
    const name: string = profile.getName();
    const email: string = profile.getEmail();

    this.globalService.setLoadingSpinner(true);
    const sub = this._authService.loginByGoogle({
      'googleId': googleId,
      'name': name,
      'email': email
    }).subscribe((res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this._authService.saveUserInto(res.data.entries[0]);
          this._authService.saveAccessToken(res.data.meta.token);
          window.location.reload();
        } else if (res.status === HTTP_CODES.CREATED) {
          this.swalDialogService.openSuccess(res.messages.join('.'));
          this.googleId = googleId;
          this.showPhonePopup = true;
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
        this.globalService.setLoadingSpinner(false);
      }
    );

    this.subscriptions.push(sub);
  }

  sendOTPCode() {
    if (this.phoneInput) {
      this.registerPhone = this.phoneInput;
      this.globalService.setLoadingSpinner(true);
      if (this.googleId !== null) {
        const sub = this._authService.confirmPhoneGoogleAccount(this.registerPhone, this.googleId)
          .subscribe((res: any) => {
            this.globalService.setLoadingSpinner(false);
            if (res.status === HTTP_CODES.SUCCESS) {
              this.showOTPPopup = true;
              this.showPhonePopup = false;
              this.swalDialogService.openSuccess(res.messages.join('.'));
            } else {
              this.registerPhone = '';
              this.swalDialogService.openError(res.messages.join('.'));
            }
          });
        this.subscriptions.push(sub);
      } else {
        const sub = this._authService.confirmPhoneFacebookAccount(this.registerPhone, this.facebookId)
          .subscribe((res: any) => {
            this.globalService.setLoadingSpinner(false);
            if (res.status === HTTP_CODES.SUCCESS) {
              this.showOTPPopup = true;
              this.showPhonePopup = false;
              this.swalDialogService.openSuccess(res.messages.join('.'));
            } else {
              this.registerPhone = '';
              this.swalDialogService.openError(res.messages.join('.'));
            }
          });
        this.subscriptions.push(sub);
      }
    }
  }

  confirmAccountByOTPCode() {
    this.globalService.setLoadingSpinner(true);
    const sub = this._authService.confirmAccountByOTPCode(this.registerPhone, this.otpInput)
      .subscribe((res: any) => {
        this.globalService.setLoadingSpinner(false);
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess(res.messages.join('.'));
          this._authService.saveUserInto(res.data.entries[0]);
          this._authService.saveAccessToken(res.data.meta.token);
          window.location.reload();
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
      });

    this.subscriptions.push(sub);
  }

  resendOTPCode() {
    this.globalService.setLoadingSpinner(true);
    const sub = this._authService.requestResendOTPCode(this.registerPhone)
      .subscribe((res: any) => {
        this.globalService.setLoadingSpinner(false);
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess(res.messages.join('.'));
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
      });

    this.subscriptions.push(sub);
  }

  onLoggedIn() {
    this.showOTPPopup = false;
    this.showAuthPopupChange.emit(false);
  }

  private googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: environment.googleAuth2ClientID,
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignIn(this.googleBtn.nativeElement);
    });
  }

  private facebookInit() {
    (window as any).fbAsyncInit = () => {
      FB.init({
        appId: '2293629277515924',
        cookie: true,
        xfbml: true,
        version: 'v3.2'
      });
      FB.AppEvents.logPageView();
    };

    ((d, s, id) => {
      let js;
      const fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s);
      js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
  }
}
