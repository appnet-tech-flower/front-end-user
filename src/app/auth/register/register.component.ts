import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { BasePage } from '../../shared/components/base/base-page';
import { MessageService } from 'primeng/api';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';

export interface IResRegister {
  status: number;
  messages: string[];
  data: {
    meta: {},
    entries: []
  };
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [MessageService]
})
export class RegisterComponent extends BasePage implements OnInit {
  registerForm: FormGroup;
  genderRadio = GlobalConstant.Gender;
  private _initFormValue = null;

  @Output() registered = new EventEmitter();

  constructor(private _authService: AuthService,
    private _fb: FormBuilder,
  ) {
    super();
  }

  ngOnInit() {
    this.initRegisterForm();
  }

  public initRegisterForm() {
    this.registerForm = this._fb.group({
      email: ['', [
        this.validatorService.checkRequired(),
        this.validatorService.checkEmail()
      ]],
      name: ['', [this.validatorService.checkRequired()]],
      phone: ['', [this.validatorService.checkRequired(), this.validatorService.checkNumberic()]],
      password: ['', [this.validatorService.checkRequired()]],
      confirmedPassword: ['', [this.validatorService.checkRequired()]],
      gender: [this.genderRadio.GENDER_MALE, this.validatorService.checkRequired()]
    }, { validators: this.validatorService.checkConfirmPassword() });

    this._initFormValue = JSON.parse(JSON.stringify(this.registerForm.value));
  }

  public onResetForm() {
    this.registerForm.reset(this._initFormValue);
    this.markAsUntouchedForAll(this.registerForm);
  }

  public onRegister() {
    this.markAsTouchedForAll(this.registerForm);
    const countError = this.getNumberOfFormErrors(this.registerForm);
    if (countError > 0) {
      return;
    }

    const sub = this._authService.register(this.registerForm.value).subscribe(
      (res: IResRegister) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.registered.emit(this.registerForm.value.phone);
          // this.initMessage(res, 'Đăng ký tài khoản thành công, vui lòng kiểm tra email để kích hoạt tài khoản !');
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
      }
    );
    this.subscriptions.push(sub);
  }

  initMessage(response: IResRegister, successfulDetail: string) {

    if (response.status === HTTP_CODES.SUCCESS) {
      this.swalDialogService.openSuccess(successfulDetail);
      this.messageService.add({ severity: 'success', summary: 'Thông Báo', detail: successfulDetail, life: 10000 });
    } else {
      let errorMessage = '';
      response.messages.forEach(event => {
        errorMessage += event + '.';
      });
      this.swalDialogService.openError(errorMessage);
      this.messageService.add({ severity: 'error', summary: 'Thông Báo', detail: errorMessage, life: 10000 });
    }
  }
}
