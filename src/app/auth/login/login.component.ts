import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { BasePage } from '../../shared/components/base/base-page';
import { AuthService } from '../../shared/services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MessageService, DialogService } from 'primeng/api';
import { GlobalService } from '../../shared/services/global.service';
import { CookieService } from 'ngx-cookie-service';
import { SocketMessageService } from '../../shared/services/socket-message.service';
import { PopupForgotPasswordComponent } from '../popup-forgot-password/popup-forgot-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})


export class LoginComponent extends BasePage implements OnInit {
  loginForm: FormGroup;

  @Output() loggedIn = new EventEmitter();

  constructor(private _authService: AuthService,
              private _router: Router,
              private _fb: FormBuilder,
              public globalService: GlobalService,
              private _cookieService: CookieService,
              private _socketMessageService: SocketMessageService,
              private _dialogService: DialogService) {
    super();
  }

  ngOnInit() {
    this.initLoginForm();
  }

  initLoginForm() {
    this.loginForm = this._fb.group({
      email: ['', [this.validatorService.checkRequired()]],
      password: ['', [this.validatorService.checkRequired()]]
    });
  }

  onLogin() {
    this.globalService.setLoadingSpinner(true);
    const sub = this._authService.login(this.loginForm.value)
      .subscribe((res: any) => {
        if (res.status !== HTTP_CODES.SUCCESS) {
          this.globalService.setLoadingSpinner(false);
          return this.swalDialogService.openError('Sai username hoặc mật khẩu');
        }

        const token = res.data.meta.token;
        this._authService.saveAccessToken(token);
        const userInfo = res.data.entries[0];
        this._socketMessageService.joinRoom();
        this._authService.saveUserInto(userInfo);
        this.globalService.setLoadingSpinner(false);

        if (this._router.url.includes('chi-tiet-san-pham')) {
          const data = JSON.parse(this._cookieService.get('productDetail'));
          if (this._cookieService.get('isBuyNow') === 'true') {
            this.orderService.addProductToCart(data.product, data.quantity, data.shopInfo, () => {
              // this._refDialog.close();
              this._router.navigate([`mua`]);
              this._cookieService.delete('isBuyNow');
            });
          } else {
            this.orderService.addProductToCart(data.product, data.quantity, data.shopInfo, () => {
              this.loggedIn.emit();
              // window.location.reload();
            });
          }
          this._cookieService.delete('productDetail');
        } else {
          this.loggedIn.emit();
          // window.location.reload();
        }
      });

    this.subscriptions.push(sub);
  }

  show() {
    this._dialogService.open(PopupForgotPasswordComponent, {
      showHeader: false,
      width: '80%',
      style: {
        'max-width': '600px'
      },
      contentStyle: {
        'max-height': '50%',
        overflow: 'auto'
      }
    });
  }

  onForgotPassword() {
    // this._refDialog.close();
    this.show();
  }
}
