import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { distinctUntilChanged } from 'rxjs/operators';
import { BasePage } from './shared/components/base/base-page';
import { SocketMessageService } from './shared/services/socket-message.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BasePage implements OnInit, AfterViewInit {
  title = 'front-end-user';
  breadcrumbItems: MenuItem[] = [];

  @ViewChild('spinner') spinner: ElementRef;

  constructor(private _socketMessageService: SocketMessageService) {
    super();
  }

  mobileMenuActive: boolean;

  onMobileMenuButton(event) {
    this.mobileMenuActive = !this.mobileMenuActive;
    event.preventDefault();
  }

  hideMobileMenu(event) {
    this.mobileMenuActive = false;
    event.preventDefault();
  }

  ngOnInit(): void {
    this.renderBreadcrumb();
    this.authService.init();
    if(this.authService.userInfo){
      this._socketMessageService.joinRoom();
    }
  }

  ngAfterViewInit(): void {
    this.globalService.getLoadingSpinner()
      .subscribe((value: boolean) => {
        if (value) {
          this.spinner.nativeElement.style.display = 'flex';
        } else {
          this.spinner.nativeElement.style.display = 'none';
        }
      });
  }

  private renderBreadcrumb() {
    this.globalService.getBreadcrumbs()
      .pipe(
        distinctUntilChanged()
      )
      .subscribe((items: MenuItem[]) => {
        this.breadcrumbItems = items;
      });
  }
}
