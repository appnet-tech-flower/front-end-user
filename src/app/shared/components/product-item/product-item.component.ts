import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Product } from '../../../detail-product/shared/models/Product.model';
import * as moment from 'moment';
import { GlobalConstant } from '../../constants/global.constant';
import { GlobalService } from '../../services/global.service';
import SizeImage = GlobalConstant.SizeImage;

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})

export class ProductItemComponent implements OnInit, OnDestroy {
  private _product: Product = null;
  SizeImage = SizeImage;
  @Input()
  set product(value: Product) {
    this.mapImage(value);
    this._product = value;
  }

  get product(): Product {
    return this._product;
  }

  @Input() showSaleOff = false;

  startDate: any;
  currentDate: any;
  endDate: any;
  day: any;
  hour: any;
  minute: any;
  second: any;

  interval: any;
  countdown: number;

  // dump image
  images = [
    '/assets/demo/img-1.jpg',
    '/assets/demo/img-2.jpg'
  ];

  constructor(private globalService: GlobalService) {
  }

  ngOnInit() {
    this.startDate = new Date(this.product.saleOff.startDate).getTime();
    this.currentDate = Date.now();
    this.endDate = new Date(this.product.saleOff.endDate).getTime();

    this.initCountDown();
  }

  initCountDown() {
    if (this.globalService.isPlatformBrowser() && this.showSaleOff) {
      const diff = this.endDate - this.currentDate;
      let duration = moment.duration(diff, 'milliseconds');

      const interval = 1000;

      this.interval = setInterval(() => {
        duration = moment.duration(duration.asMilliseconds() - interval, 'milliseconds');
        this.day = duration.days();
        this.hour = duration.hours();
        this.minute = duration.minutes();
        this.second = duration.seconds();

        this.countdown = Math.round(100 - ((this.endDate - this.startDate) * 100) / this.currentDate);
      }, interval);
    }
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  mapImage(product: Product) {

    product.thumbnail = product.images[0] || 'http://placehold.it/300';
  }

}
