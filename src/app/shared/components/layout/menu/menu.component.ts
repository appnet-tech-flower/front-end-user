import { Component } from '@angular/core';
import { BasePage } from '../../base/base-page';

@Component({
  selector: 'app-header-menu',
  templateUrl: './menu.component.html',
  styleUrls: [
    './menu.component.scss'
  ]
})
export class MenuComponent extends BasePage {
  menuItems = [
    {title: 'Hoa Sinh nhật', link: 'danh-muc/sinh-nhat'},
    {title: 'Hoa khai trương', link: 'danh-muc/hoa-tinh-yeu'},
    {title: 'Hoa tặng mẹ', link: 'danh-muc/hoa-tang-me'},
    {title: 'Hoa cưới', link: 'danh-muc/hoa-cuoi'},
    {title: 'Hoa tươi', link: 'danh-muc/hoa-tuoi'},
    {title: 'Hoa tình yêu', link: 'danh-muc/hoa-tinh-yeu'},
    {title: 'Hoa chia buồn', link: 'danh-muc/hoa-tang-le'},
    {title: 'Quà Tặng', link: 'danh-muc/qua-tang'},
    {title: 'Bánh kem', link: 'danh-muc/banh-kem'},
    {title: 'Combo', link: 'danh-muc/combo'},
    {title: 'Tin tức', link: 'tin-tuc'}
  ];
}
