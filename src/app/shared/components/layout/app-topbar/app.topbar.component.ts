import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService, MessageService } from 'primeng/api';
import { Dialog } from 'primeng/dialog';
import { GlobalConstant } from '../../../constants/global.constant';
import { AuthService, ILoggedInUser } from '../../../services/auth.service';
import { BasePage } from '../../base/base-page';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './app.topbar.component.html',
  styleUrls: ['./app.topbar.component.scss'],
  providers: [DialogService, MessageService]
})
export class AppTopBarComponent extends BasePage implements OnInit {
  userInfo: ILoggedInUser = null;
  isLogin = false;
  orderItems: any[] = [];
  userType = GlobalConstant.UserType;
  type: number;
  showAuthPopup = false;

  @ViewChild('authPopup') authPopup: Dialog;

  constructor(
    public messageService: MessageService,
    private _authService: AuthService,
    private _router: Router,
    public globalService: GlobalService
  ) {
    super();
  }

  ngOnInit() {
    this._authService.getUserInfo()
      .subscribe((userInfo: any) => {
        this.isLogin = !!userInfo;

        if (this.isLogin) {
          this.userInfo = userInfo;
          this.type = userInfo.type;
        }
      });

    this.orderService.getProductsInCart()
      .subscribe((orderItems: any[]) => {
        this.orderItems = orderItems;
      });
  }

  onLogout() {
    this.globalService.setLoadingSpinner(true);
    this.orderService.setProductsInCarts([]);

    setTimeout(() => {
      this._authService.logout();
      this._authService.saveUserInto(null);
      this._authService.saveAccessToken('');
      this._router.navigate(['/']);
      this.globalService.setLoadingSpinner(false);
    }, 500);
  }

  show() {
    this.showAuthPopup = true;
  }

  onUserAction() {
    if (this.isLogin) {
      this._router.navigate(['/trang-ca-nhan']);
    } else {
      this.show();
    }
  }

  resetCenterPopup() {
    if (this.showAuthPopup) {
      setTimeout(() => {
        this.authPopup.center();
      })
    }
  }
}
