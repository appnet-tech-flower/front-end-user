import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-text-end-page',
  templateUrl: './text-end-page.component.html',
  providers: []
})

export class TextEndPageComponent {

  @Input() textEndPage: string = '';

}
