import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabViewModule, ButtonModule } from 'primeng/primeng';
import { ProgressBarModule } from 'primeng/progressbar';
import { ReactiveFormsModule } from '@angular/forms';
import { NewCardItemComponent } from './new-card-item.component';
import { AppStaticImageModule } from '../../pipes/app-static-image/app-static-image.module';

@NgModule({
  declarations: [NewCardItemComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    TabViewModule,
    ReactiveFormsModule,
    ButtonModule,
    ProgressBarModule,
    RouterModule,
    AppStaticImageModule
  ],
  exports: [
    NewCardItemComponent
  ],
  providers: [
  ]
})

export class NewCardItemModule {

}
