import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { GlobalConstant } from '../../constants/global.constant';
import SizeImage = GlobalConstant.SizeImage;

export interface NewItem {
  image: string;
  url: string;
  title: string;
  content: string;
  description: string;
  createdAt: Date;
}
@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.scss']
})

export class NewItemComponent implements OnInit {
  private _news: NewItem = {
    image: 'assets/images/project-1.jpg',
    url: '',
    title: 'title tin tuc',
    content: 'content tin tuc',
    description: 'description tin tuc',
    createdAt: new Date(),
  };


  SizeImage = SizeImage;

  @Input()
  set news(value: any) {
    this._news = value;
  }

  get news(): any {
    return this._news;
  }

  constructor() {
  }

  ngOnInit() {
  }
}
