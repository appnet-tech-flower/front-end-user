import { HttpClient } from '@angular/common/http';
import {
  AfterContentInit,
  Component,
  DebugElement,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  Output, ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { API } from '../../constants/api.constant';
import { GlobalConstant } from '../../constants/global.constant';
import { StringService } from '../../services/string.service';
import { BaseComponent } from '../base/base.component';
import { forkJoin } from 'rxjs';

const IMAGE_UPLOADER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => ImageUploaderComponent),
  multi: true
};

interface IFile {
  elId?: string;
  file?: any;
  percentUploaded?: number;
  result: any;
  url: string;
  isDemo?: boolean;
  isValid: boolean;
}

export interface IFileTextValue {
  link: string;
  isDemo?: boolean;
  isValid?: boolean;
}

@Component({
  templateUrl: './imageUploader.component.html',
  selector: 'app-image-uploader',
  styleUrls: ['./imageUploader.component.scss'],
  providers: [IMAGE_UPLOADER_VALUE_ACCESSOR]
})

export class ImageUploaderComponent extends BaseComponent implements AfterContentInit, ControlValueAccessor {
  _files: IFile[] = [];
  _values = [];
  invalidImage = false;

  GlobalConstant = GlobalConstant;
  environment = environment;

  @Input()
  set values(values: IFileTextValue[]) {
    this._values = values;
    this._files = [];

    values.forEach((link: IFileTextValue) => {
      if (link.isDemo === undefined) {
        link.isDemo = true;
      }

      this._files.push({
        elId: StringService.guidGenerator(),
        file: {},
        percentUploaded: 0,
        result: {
          link: link.link
        },
        url: this.getLinkImage(link),
        isDemo: link.isDemo,
        isValid: true
      });
    });
  }

  get values(): IFileTextValue[] {
    return this._values;
  }

  @Input() max = -1;
  @Input() maxSize = 1024 * 2; // 2mb
  @Output() valueChanged = new EventEmitter<IFileTextValue[]>();

  @ViewChild('inputFile') inputFile: DebugElement;

  constructor(private el: ElementRef,
              private http: HttpClient) {
    super();
  }

  ngAfterContentInit() {

  }

  writeValue(obj: any): void {
    this.values = obj;
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onModelTouched = fn;
  }

  updateModel() {
    this.values = this._files
      .map(f => {
        return {
          link: f.result ? f.result.link : '',
          isDemo: f.isDemo,
          isValid: f.isValid
        };
      });

    this.valueChanged.emit(this.values);
    this.onModelChange(this.values);
  }

  onChangeInputFiles(event: any) {
    let selectedFiles = event.target.files;
    if (selectedFiles.length === 0) {
      return;
    }

    selectedFiles = this._mapFileListToArray(selectedFiles);

    if (this._existFileTooLarge(selectedFiles)) {
      this.resetValueOfInput();
      return;
    }

    if (this.max !== -1 && this._files.length + selectedFiles.length > this.max) {
      selectedFiles = selectedFiles.slice(0, this.max);
      this.resetValueOfInput();
    }

    const waitingImages = this.generateObjectImage(selectedFiles);
    this._files = [
      ...waitingImages,
      ...this._files
    ];

    if (waitingImages.length > 0) {
      setTimeout(() => {
        this._renderNewImages(waitingImages).subscribe(
          {
            next: resList => {
              this.invalidImage = false;
              resList.map(res => {
                if (res.status === 1) {
                  const file = {
                    url: environment.staticImageDemo + res.data.link.toString(),
                    result: res.data,
                    isDemo: true,
                    isValid: true
                  };
                  this._files.push(file);
                } else {
                  const file = {
                    url: null,
                    result: null,
                    isDemo: true,
                    isValid: false
                  };
                  this.invalidImage = true;
                  this._files.push(file);
                }
              });
            },
            complete: () => {
              this._files = this._files.filter(file => file.isValid);
              this.updateModel();
            },
          }
        );
      }, 100);
      this.resetValueOfInput();
    }
  }

  trackBy(index, item) {
    return item.elId;
  }

  removeFile(file: IFile) {
    const index = this.findIndexFile(file);
    if (index !== -1) {
      this._files.splice(index, 1);
      this.updateModel();
    }
  }

  private onModelChange = (_: any) => {
  }

  private onModelTouched = () => {
  }

  private generateObjectImage(files: any): IFile[] {
    const results: IFile[] = [];

    for (const key in files) {
      if (files.hasOwnProperty(key)) {
        results.push({
          elId: StringService.guidGenerator(),
          file: files[key],
          percentUploaded: 0,
          result: null,
          isValid: false,
          url: ''
        });
      }
    }

    return results;
  }

  private findIndexFile(file: IFile) {
    return this._files.findIndex(f => f.elId === file.elId);
  }

  private resetValueOfInput() {
    this.inputFile.nativeElement.value = '';
  }

  private _renderNewImages(newImages: IFile[]) {
    const observableBatch = [];
    newImages.forEach(imgFile => {
      observableBatch.push(this._uploadImage(imgFile));
    });
    return forkJoin(observableBatch);
  }

  private _existFileTooLarge(selectedFiles: any): boolean {
    // TODO:
    return false;
  }

  private _uploadImage(imgFile: IFile) {
    const formData = new FormData();
    formData.append('file', imgFile.file);

    return this.http.post(API.uploadImage, formData);
  }

  private getLinkImage(link: IFileTextValue): string {

    if (link.link.indexOf('http://') >= 0 || link.link.indexOf('https://') >= 0) {
      return link.link;
    }

    return link.isDemo ?
      environment.staticImageDemo + link.link :
      environment.staticImageSize + GlobalConstant.SizeImage.S400 + '/' + link.link;
  }

  private _mapFileListToArray(files: FileList): File[] {
    const results: File[] = [];
    for (let i = 0; i < files.length; i++) {
      results.push(files.item(i));
    }

    return results;
  }
}
