import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsListComponent } from './news-list.component';
import { NewsListService } from './news-list.service';
import { FormsModule } from '@angular/forms';
import { NewItemModule } from '../new-item/new-item.module';
import { CardModule } from 'primeng/card';
import { NewCardItemModule } from '../new-card-item/new-card-item.module';

@NgModule({
  declarations: [NewsListComponent],
  imports: [
    CommonModule,
    CommonModule,
    FormsModule,
    NewCardItemModule,
    CardModule
  ],
  exports: [
    NewsListComponent
  ],
  providers: [
    NewsListService
  ]
})
export class NewsListModule { }
