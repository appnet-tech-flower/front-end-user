import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SearchSelector } from '../../constants/search-selector.constant';
import { BasePage } from '../base/base-page';
import {SearchSelectorService} from '../../services/search-selector.service';

import Topics = SearchSelector.Topics;
import SpecialOccasions = SearchSelector.SpecialOccasions;
import Designs = SearchSelector.Designs;
import Florets = SearchSelector.Florets;
import Colors = SearchSelector.Colors;
import PriceRanges = SearchSelector.PriceRanges;
import Cities = SearchSelector.Cities;

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent extends BasePage implements OnInit {
  searchForm: FormGroup;
  isFreshFlower = false;
  topics: Array<any> = Topics;
  specials: Array<any> = SpecialOccasions;
  designs: Array<any> = Designs;
  florets: Array<any> = Florets;
  colors: Array<any> = Colors;
  price: Array<any> = PriceRanges;
  cities: Array<any> = [];
  districts: Array<any> = [];
  private _data: any = {};

  @Output() valueChange = new EventEmitter<any>();

  @Input()
  set data(value: any) {
    if (value !== null && value !== undefined && value !== '') {
      this._data = SearchSelectorService.mapDataForSearchBox(value);
      this._mapDataToForm();
    }
  }

  constructor(private _fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.searchForm = this._fb.group({
      topic: [null],
      specialOccasion: [null],
      design: [null],
      floret: [null],
      color: [null],
      priceRange: [null],
      city: [null],
      district: [null]
    });
    this.initCity();
  }

  onSearch() {
    const dataToEmit = this._mapValueForEmitting();
    if (Object.keys(dataToEmit).length !== 0) {
      this.valueChange.emit(dataToEmit);
    }
  }

  onChangeCity(event): void {
    this.districts = event ? event.districts : [];
    this.searchForm.controls.district.setValue(null);
  }

  onTopicChanged(event): void {
    if (event && event.value === 9) {
      this.isFreshFlower = true;
    } else {
      this.isFreshFlower = false;
      this.searchForm.controls.specialOccasion.setValue(null);
    }
  }

  private _mapValueForEmitting(): any {
    const formValue = this.searchForm.value;
    const data: any = {};
    if (formValue.topic) {
      data.topic = formValue.topic.value;
    }

    if (formValue.specialOccasion) {
      data.specialOccasion = formValue.specialOccasion.value;
    }

    if (formValue.design) {
      data.design = formValue.design.value;
    }

    if (formValue.floret) {
      data.floret = formValue.floret.value;
    }

    if (formValue.color) {
      data.color = formValue.color.value;
    }

    if (formValue.priceRange) {
      data.priceRange = formValue.priceRange.value;
    }

    if (formValue.city) {
      data.city = formValue.city.code;
    }

    if (formValue.district) {
      data.district = formValue.district.id;
    }

    return data;
  }

  private _mapDataToForm() {
    setTimeout(() => {
      this.searchForm.patchValue(this._data);
    });
  }

  private initCity() {
    this.cities = Cities.filter((city) => city.code === 'SG');
  }

}
