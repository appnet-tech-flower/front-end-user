import { Component, ElementRef, EventEmitter, OnInit, forwardRef, Input, NgZone, Output, ViewChild } from '@angular/core';
import { GeoCodingApiService } from '../../services/geo-coding-api.service';
import { MapsAPILoader } from '@agm/core';
import { FormControl } from '@angular/forms';

import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

declare const google: any;

export interface IMap {
  longitude: number;
  latitude: number;
  address: string;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => MapComponent)
    }
  ]
})

export class MapComponent implements OnInit, ControlValueAccessor {
  _location = {
    latitude: 10.8230989,
    longitude: 106.6296638
  };
  windowInfo = '';
  isErrorAddress = false;
  @Output() valueChanged = new EventEmitter<IMap | null>(null);
  public searchControl: FormControl = new FormControl();
  @ViewChild('search')
  public searchElementRef: ElementRef;
  private onModelChange = (_: any) => {
  }
  private onModelTouched = () => {
  }

  constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private geoService: GeoCodingApiService) {
  }

  _address = '';

  get address(): string {
    return this._address;
  }

  @Input()
  set address(value: string) {
    if (this._address !== value) {
      this._address = value;
      this._getCoordsFromAddress();
    }
  }

  @Input()
  set coords(value) {
    this._location = {
      latitude: value.latitude,
      longitude: value.longitude
    };
  }

  get value(): string {
    return this._address;
  }

  @Input()
  set value(v: string) {
    this._address = v;
    this.searchElementRef.nativeElement.value = v;
    this.windowInfo = v;
  }

  writeValue(obj: any) {
    this.value = obj;
  }

  ngOnInit() {
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        componentRestrictions: {country: 'VN'},
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          // set latitude, longitude and zoom
          this._location.latitude = place.geometry.location.lat();
          this._location.longitude = place.geometry.location.lng();
          this.address = place.formatted_address;
          this.windowInfo = `<strong>Địa chỉ: </strong> ${place.formatted_address}`;
          this.emitAddressByMarker();
        });
      });
    });
  }

  markerDragEnd(value: any) {
    this._getAddressFromCoords(value.coords);
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onModelTouched = fn;
  }

  private _getCoordsFromAddress() {
    this.geoService.findFromAddress(this._address)
        .subscribe(res => {
          if (res.status === 'OK' && res.results && res.results.length > 0) {
            this._location.latitude = res.results[0].geometry.location.lat;
            this._location.longitude = res.results[0].geometry.location.lng;
            this.address = res.results[0].formatted_address;
            this.windowInfo = `<strong>Địa chỉ: </strong> ${this.address}`;
            this.emitAddressByMarker();
            this.isErrorAddress = false;
          }
        });
  }

  private _getAddressFromCoords(coords: any) {
    console.log(coords);
    this._location.latitude = coords.lat;
    this._location.longitude = coords.lng;

    this.geoService.findFromLatLng(coords.lat, coords.lng)
        .subscribe(res => {
          console.log(res);
          if (res.status === 'OK' && res.results && res.results.length > 0) {
            this.searchElementRef.nativeElement.value = res.results[0].formatted_address;
            this.address = res.results[0].formatted_address;
            this.windowInfo = `<strong>Địa chỉ: </strong> ${this.address}`;
            this.emitAddressByMarker();
            this.isErrorAddress = false;
          }
        });
  }

  private emitAddressByMarker() {
    if (this.isErrorAddress) {
      this.valueChanged.emit(null);
    } else {
      this.valueChanged.emit({
        address: this.address,
        longitude: this._location.longitude,
        latitude: this._location.latitude
      });
      this.onModelChange(this.address);
    }
  }
}

