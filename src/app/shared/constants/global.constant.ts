export namespace GlobalConstant {

  export namespace UserType {
    export const NormalUser = 1;
    export const UserAsShop = 2;
  }

  export namespace SizeImage {
    export const S400 = '400';
    export const S600 = '600';
    export const S800 = '800';
    export const S1000 = '1000';
    export const S1200 = '1200';
    export const S1400 = '1400';
  }

  export namespace Gender {
    export const GENDER_MALE = 1;
    export const GENDER_FEMALE = 2;
  }

  export const Status = {
    ACTIVE: 1,
    PENDING_OR_WAIT_CONFIRM: 2,
    BLOCKED: 3,
    DELETE: 4,
    PRODUCT_HIDDEN: 5,

    CHILD_ACCEPTED: 8,
    CHILD_WAITING: 9,
    CHILD_REJECTED: 10,
    CHILD_DELETED: 11,
    CHILD_NONE: 12,
    PAID_FORM_VIEW_ACTIVE: 40,
    PAID_FORM_VIEW_STOP: 50,
    OUT_OF_STOCK: 60,

    NOTIFY_NEW: 200,
    NOTIFY_READ: 201,

    ORDER_PENDING: 1000,
    ORDER_NOT_YET_PAID: 1001,
    ORDER_SUCCESS: 1002,
    ORDER_CONFIRMED: 1003,
    ORDER_PAID: 1004,

    ORDER_ITEM_NEW: 2000,
    ORDER_ITEM_PROCESSING: 2001,
    ORDER_ITEM_ON_DELIVERY: 2002,
    ORDER_ITEM_FINISHED: 2003,
    PRODUCT_PENDING_APPROVE: 3000,
    PRODUCT_APPROVED: 3001,
    PRODUCT_NOT_APPROVED: 3002,
  };

  export namespace ProductStatus {
    export const ACTIVE = 1;
    export const HIDDEN = 65;
  }

  export namespace Bank {
    export const ACB = '/assets/layout/images/bank-acb.png';
    export const DONGA = '/assets/layout/images/bank-donga.png';
  }
  export namespace Messages {
    export const WaitingForApprovedMessages = 'Vui lòng đợi xét duyệt cửa hàng của bạn. ' +
      'Hiện tại tài khoản được kích hoạt bạn có thể mua hàng.';
  }
}
