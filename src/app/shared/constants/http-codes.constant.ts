export namespace HTTP_CODES {
  export const SUCCESS = 200;
  export const ERROR = 400;
  export const CREATED = 201;
  export const ERROR_NOT_FOUND = 404;
  export const ERROR_AUTHORIZED = 401;
}
