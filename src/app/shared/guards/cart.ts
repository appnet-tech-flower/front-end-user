import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderService } from '../services/order.service';

@Injectable()
export class CartCanActive implements CanActivate {
  constructor(private router: Router,
              private orderService: OrderService) {

  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.orderService.getDeliveryInfo().subscribe(data => {
      if (Object.entries(data).length === 0 && data.constructor === Object) {
        this.router.navigate(['']);
        return false;
      }
    });
    return true;
  }

}
