import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Product } from '../../detail-product/shared/models/Product.model';
import { ShopInfo } from '../../detail-product/shared/models/Shop.model';
import { API } from '../constants/api.constant';
import { HTTP_CODES } from '../constants/http-codes.constant';
import { IDeliveryInfo } from '../../checkout/shared/models/delivery.model';
import { GlobalService } from './global.service';
import { TokenStorage } from './token-storage.service';
import { CookieService } from 'ngx-cookie-service';

export interface IOrderItem {
  _id?: string;
  quantity: number;
  product: Product;
  shop?: ShopInfo;
  order?: string;
}

export interface ICookieProducts {
  productId: string;
  quantity: number;
}

const dumpResponseSuccess = {status: HTTP_CODES.SUCCESS, data: []};

@Injectable()
export class OrderService {
  private _productsInCart$ = new BehaviorSubject<IOrderItem[]>([]);

  private _deliveryInfo$ = new BehaviorSubject<any>({});

  constructor(private httpClient: HttpClient,
              private cookieService: CookieService,
              private globalService: GlobalService,
              private tokenStorage: TokenStorage) {
  }

  getDeliveryInfo(): Observable<IDeliveryInfo> {
    return this._deliveryInfo$.asObservable();
  }

  setDeliveryInfo(info: any) {
    this._deliveryInfo$.next(info);
  }

  getProductsInCart(): Observable<IOrderItem[]> {
    return this._productsInCart$.asObservable();
  }

  setProductsInCarts(items: IOrderItem[]) {
    this._productsInCart$.next(items);
  }

  // use in case list product in cart
  public updateProductQuantity(product: Product, quantity: number) {
    const currentOrderItems = this._productsInCart$.getValue();

    if (!this.tokenStorage.getAccessToken()) {
      currentOrderItems.forEach(orderItem => {
        if (orderItem.product._id === product._id) {
          orderItem.quantity = quantity;
        }
      });

      this.setProductsInCarts(currentOrderItems);
      this.saveListProductsToCookie();
    } else {
      this.addProductToCart(product, quantity);
    }
  }

  private addProductToCartCaseNotLogin(product: Product, quantity: number, shop?: ShopInfo, callback?: any) {
    const currentProducts = this._productsInCart$.getValue();
    let isDuplicated = false;
    currentProducts.forEach(orderItem => {
      if (orderItem.product._id === product._id) {
        orderItem.quantity += quantity;
        isDuplicated = true;
      }
    });

    if (!isDuplicated) {
      currentProducts.push({
        product,
        shop,
        quantity
      });
    }

    this.setProductsInCarts(currentProducts);
    // update cookie
    this.saveListProductsToCookie();

    // callback
    if (callback) {
      callback();
    }
  }

  addProductToCart(product: Product, quantity: number, shop?: ShopInfo, callback?: any) {

    if (!this.tokenStorage.getAccessToken()) {
      return this.addProductToCartCaseNotLogin(product, quantity, shop, callback);
    }

    const currentOrderItems = this._productsInCart$.getValue();
    const duplicatedProductById: IOrderItem = currentOrderItems.find((orderItem: IOrderItem) => {
      return orderItem.product._id === product._id;
    });

    if (duplicatedProductById) {
      this.updateQuantityItem(duplicatedProductById._id, quantity)
        .subscribe((response: any) => {
          if (response.status !== HTTP_CODES.SUCCESS) {
            if (callback) {
              callback(response);
            }
            return;
          }

          duplicatedProductById.quantity = quantity;
          this._productsInCart$.next(currentOrderItems);
          if (callback) {
            callback(response);
          }
        });
    } else {
      this._addProductToCart(product._id, quantity)
        .subscribe((response: any) => {
          if (response.status !== HTTP_CODES.SUCCESS) {
            if (callback) {
              callback(response);
            }
            return;
          }

          const _product = JSON.parse(JSON.stringify(product));
          currentOrderItems.push({
            _id: response.data.orderItem._id,
            product: _product,
            quantity,
            shop
          });
          this._productsInCart$.next(currentOrderItems);
          if (callback) {
            callback(response);
          }
        });
    }
  }

  removeCartItems(id: string, index: number) {
    if (this.tokenStorage.getAccessToken()) {
      this._removeCartItems(id)
        .subscribe((res: any) => {
          if (res.status !== HTTP_CODES.SUCCESS) {
            return;
          }
          const currentOrderItems = this._productsInCart$.getValue();
          currentOrderItems.splice(index, 1);
          this._productsInCart$.next(currentOrderItems);
          this.globalService.setLoadingSpinner(false);
        });
    } else {
      const currentOrderItems = this._productsInCart$.getValue();
      currentOrderItems.splice(index, 1);
      this._productsInCart$.next(currentOrderItems);
      this.saveListProductsToCookie();
    }
  }

  addDelivery(data): Observable<any> {
    return this.httpClient.post(API.Address.AddNewAddress, data);
  }

  getAllDeliveries(): Observable<any> {
    return this.httpClient.get(API.Address.GetListAddress);
  }

  updateQuantityItem(id: string, quantity: number): Observable<any> {
    return this.httpClient.put(API.Order.UpdateOrderItem.replace('{id}', id), {quantity});
  }

  getShippingCost(addressId: string): Observable<any> {
    return this.httpClient.post(API.Order.GetShippingCost, {addressId});
  }

  getNoLoginShippingCost(addressInfo: any, items: any): Observable<any> {
    console.log(addressInfo);
    console.log(items);
    return this.httpClient.post(API.Order.GetNoLoginShippingCost, {addressInfo, items});
  }
  getCartItems(): Observable<any> {
    if (!this.globalService.isPlatformBrowser()) {
      return of(dumpResponseSuccess);
    }

    if (this.tokenStorage.getAccessToken()) {
      return this.getCartItemsCaseLoggedIn();
    }

    return this.getCartItemCaseDidNotLogIn();
  }

  private getCartItemsCaseLoggedIn() {
    const productsString = this.cookieService.get('productsCookie');
    if (productsString) {
      try {
        const products: ICookieProducts[] = JSON.parse(productsString);
        if (products.length === 0) {
          this.cookieService.delete('productsCookie');
          return this.httpClient.get(API.Order.PendingOrder);
        }

        return this.httpClient.post(API.Order.AddManyProduct, {items: products})
          .pipe(
            mergeMap((res: any) => {
              if (res.status !== HTTP_CODES.SUCCESS) {
                console.error(res);
              } else {
                this.cookieService.delete('productsCookie');
              }

              return this.httpClient.get(API.Order.PendingOrder);
            })
          );
      } catch (e) {
        this.cookieService.delete('productsCookie');
        return this.httpClient.get(API.Order.PendingOrder);
      }
    }

    return this.httpClient.get(API.Order.PendingOrder);
  }

  private getCartItemCaseDidNotLogIn() {
    const productsString = this.cookieService.get('productsCookie');
    if (productsString) {
      try {
        const products: ICookieProducts[] = JSON.parse(productsString);
        if (products.length === 0) {
          return of(dumpResponseSuccess);
        }

        return this.getListProductsFromCookies(products);
      } catch (e) {
        this.cookieService.delete('productsCookie');
        return of(dumpResponseSuccess);
      }
    }

    return of({status: HTTP_CODES.SUCCESS, data: []});
  }

  private getListProductsFromCookies(products: ICookieProducts[]): Observable<any> {
    const productToQuantityObj = {};
    const productIds = products.map(p => {
      productToQuantityObj[p.productId] = p.quantity;
      return p.productId;
    });
    const params = {productIds: productIds.join(',')};

    return this.httpClient.get(API.Product.ProductInfosByIds, {params})
      .pipe(
        map((res: any) => {
          return this.mapProductInfoInToOrderItemInCart(res, productToQuantityObj);
        })
      );
  }

  private mapProductInfoInToOrderItemInCart(res: any, productToQuantityObj: { [key: string]: number }) {
    let productsInCart = [];
    if (res.status === HTTP_CODES.SUCCESS) {
      productsInCart = res.data.entries.products.map(p => {
        return {
          shop: p.shop,
          quantity: productToQuantityObj[p._id],
          product: p
        };
      });

      this.setProductsInCarts(productsInCart);
      this.saveListProductsToCookie();
    }

    return {
      status: res.status,
      data: productsInCart
    };
  }

  private _removeCartItems(id: string): Observable<any> {
    return this.httpClient.delete(API.Order.DeleteOrderItem.replace('{id}', id));
  }

  getUserOrders(): Observable<any> {
    return this.httpClient.get(API.Order.ListOrder);
  }

  getItemsOrder(id: string): Observable<any> {
    return this.httpClient.get(API.Order.GetItemsOrder.replace('{id}', id));
  }

  submitOrder(data: any): Observable<any> {
    return this.httpClient.put(API.Order.SubmitOrder, data);
  }

  submitOrderForGuest(data: any): Observable<any> {
    return this.httpClient.post(API.Order.SubmitOrderForGuest, data);
  }

  private _addProductToCart(productId: string, quantity: number): Observable<any> {
    return this.httpClient.post(API.Order.AddProductToCart, {productId, quantity});
  }

  orderStatisticForShop(params?: { startDate: string; endData: string }): Observable<any> {
    return this.httpClient.get(API.Shop.StatisticOrder, {params});
  }

  moneyStatisticForShop(params?: { startDate: string; endData: string }): Observable<any> {
    return this.httpClient.get(API.Shop.StatisticMoney, {params});
  }

  private saveListProductsToCookie() {
    const currentProducts = this._productsInCart$.getValue();
    const productsToSave: ICookieProducts[] = currentProducts.map(orderItem => {
      return {
        productId: orderItem.product._id,
        quantity: orderItem.quantity
      };
    });

    this.cookieService.set('productsCookie', JSON.stringify(productsToSave), 7, '/');
  }

  lookUpOrder(param: string): Observable<any> {
    return this.httpClient.get(API.Order.LookupOrder.replace(':code', param));
  }
}
