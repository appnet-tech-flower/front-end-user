import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class GlobalService {

  constructor(@Inject(PLATFORM_ID) private platformId) {

  }

  private breadcrumbs$ = new BehaviorSubject<MenuItem[]>([]);

  private loadingSpinner$ = new BehaviorSubject<boolean>(false);

  private toggleMobileMenu$ = new BehaviorSubject<boolean>(false);

  public isPlatformBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  public setLoadingSpinner(value: boolean) {
    this.loadingSpinner$.next(value);
  }

  public getLoadingSpinner(): Observable<boolean> {
    return this.loadingSpinner$.asObservable();
  }

  public setBreadcrumbs(values: MenuItem[]) {
    setTimeout(() => {
      this.breadcrumbs$.next(values);
    }, 100);
  }

  public getBreadcrumbs(): Observable<MenuItem[]> {
    return this.breadcrumbs$.asObservable();
  }

  public getToggleMobileMenu(): Observable<boolean> {
    return this.toggleMobileMenu$.asObservable();
  }

  public setToggleMobileMenu() {
    this.toggleMobileMenu$.next(!this.toggleMobileMenu$.getValue());
  }
}
