import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Product } from '../../detail-product/shared/models/Product.model';
import { API } from '../constants/api.constant';

export interface IResSearchBoxResult {
  status: number;
  messages: string[];
  data: {
    url: string;
  };
}

export interface IResSearchContent {
  status: number;
  messages: string[];
  data: {
    isList?: boolean;
    isDetail?: boolean;
    products?: Product[];
    product?: Product;
    relatedProducts?: Product[];
    shopInfo?: any;
    totalItems?: number;
    searchQuery?: any;
  };
}

@Injectable()
export class SearchService {

  constructor(private _http: HttpClient) {
  }

  searchBox(data: any): Observable<IResSearchBoxResult> {
    return this._http.post<IResSearchBoxResult>(API.Search.SearchBox, data);
  }

  searchContent(params: any): Observable<IResSearchContent> {
    return this._http.get<IResSearchContent>(API.Search.SearchContent, {params});
  }
}
