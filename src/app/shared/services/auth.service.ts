import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { TokenStorage } from './token-storage.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API } from '../constants/api.constant';
import { Router } from '@angular/router';

export interface ILoggedInUser {
  _id: string;
  name: string;
  email: string;
  phone: number;
  birthday: string;
  gender: number;
  role: number;
  type: number;
}

export interface IPassword {
  password: string;
  confirmedPassword: string;
  token: string;
}

@Injectable()
export class AuthService {
  token = '';
  userInfo: ILoggedInUser | null;
  private userInfo$ = new BehaviorSubject<ILoggedInUser | null>(null);

  constructor(private _http: HttpClient,
              private tokenStorage: TokenStorage,
              private router: Router) {

  }

  init() {
    this.token = this.tokenStorage.getAccessToken();
    this.userInfo = this.tokenStorage.getUserInfo();
    this.userInfo$.next(this.userInfo);

    if (!this.token || !this.userInfo) {
      this.tokenStorage.clear();
    }
  }

  public setUserInfo(value: ILoggedInUser | null) {
    this.userInfo = value;
    this.userInfo$.next(value);
  }

  public getUserInfo(): Observable<ILoggedInUser | null> {
    return this.userInfo$.asObservable();
  }

  public loginByGoogle(user: any): Observable<any> {
    return this._http.post(API.Profile.LoginByGoogle, user);
  }

  public loginByFacebook(token: string): Observable<any> {
    return this._http.post(API.Profile.LoginByFacebook, {token});
  }

  public login(user: any): Observable<any> {
    return this._http.post(API.Profile.Login, user);
  }

  public logout(): void {
    this.userInfo = null;
    this.token = '';
    this.tokenStorage.clear();
    console.log(this.tokenStorage);
  }

  public register(user: any): Observable<any> {
    return this._http.post(API.Profile.Register, user);
  }

  public forgotPassword(email: string): Observable<any> {
    let query = new HttpParams();
    query = query.append('email', email);
    return this._http.get(API.Profile.ForgotPassword, {params: query});
  }

  public resetPassword(password: IPassword) {
    return this._http.post(API.Profile.ResetPassword, password);
  }

  public saveAccessToken(token: string) {
    this.tokenStorage.setAccessToken(token);
  }

  public saveUserInto(userInfo: any) {
    this.userInfo = userInfo;
    this.userInfo$.next(userInfo);
    this.tokenStorage.setUserInfo(userInfo);
  }

  checkTokenWhenInitApp(): Promise<any> {
    this.init();

    return new Promise((resolve => {
      resolve();
    }));
  }

  public confirmAccount(token: string): Observable<any> {
    return this._http.get(API.Account.ConfirmAccount, {params: {token}});
  }

  public resendConfirmAccount(email: string): Observable<any> {
    return this._http.post(API.Account.ResendConfirmAccount, email);
  }

  public requestResendOTPCode(phone: string) {
    return this._http.post(API.Profile.RequestSesendOTP, {phone});
  }

  public confirmAccountByOTPCode(phone: string, otp: string) {
    return this._http.post(API.Profile.ConfirmAccountByOTP, {phone, otp});
  }
  public confirmPhoneGoogleAccount(phone: string, googleId: number) {
    return this._http.post(API.Profile.ConfirmPhoneGoogleAccount, {phone, id: googleId});
  }
  public confirmPhoneFacebookAccount(phone: string, facebookId: number) {
    return this._http.post(API.Profile.ConfirmPhoneFacebookAccount, {phone, id: facebookId});
  }
}
