import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable()
export class SeoService {

  constructor(private title: Title) {
  }

  setHeader(title: string) {
    let newTitle = title;
    if (newTitle === '') {
      newTitle = 'Dịch vụ điện hoa | Giao hoa 63 tỉnh thành';
    }
    newTitle += ' - FLOWERSHOP.VN';
    this.title.setTitle(newTitle);
  }
}
