import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable()
export class SwalDialogService {

  openInfo(message: string, duration?: number) {
    return Swal.fire({
      text: message,
      type: 'info',
      timer: duration
    });
  }

  openError(message: string, duration?: number) {
    return Swal.fire({
      text: message,
      type: 'error',
      timer: duration
    });
  }

  openWarning(message: string, duration?: number) {
    return Swal.fire({
      text: message,
      type: 'warning',
      timer: duration
    });
  }

  openSuccess(message: string, duration?: number) {
    return Swal.fire({
      text: message,
      type: 'success',
      timer: duration
    });
  }
}
