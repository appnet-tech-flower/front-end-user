import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../home/products/shared/models/product.model';

@Component({
  selector: 'app-popular-products',
  templateUrl: './popular-products.component.html',
  styleUrls: ['./popular-products.component.scss']
})
export class PopularProductsComponent implements OnInit {

  @Input() products: Array<Product> = [];

  constructor() {
  }

  ngOnInit(): void {
  }

}
