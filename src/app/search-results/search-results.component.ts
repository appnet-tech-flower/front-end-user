import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Paginator } from 'primeng/primeng';
import { BasePage } from '../shared/components/base/base-page';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { IResSearchBoxResult, IResSearchContent, SearchService } from '../shared/services/search.service';
import { SearchSelectorService } from '../shared/services/search-selector.service';
import { SeoService} from '../shared/services/seo.service';

const PRICE_OPTIONS = [
  {label: 'Không chọn', value: {sb: '', sd: ''}},
  {label: 'Từ thấp đến cao', value: {sb: 'originalPrice', sd: 'ASC'}},
  {label: 'Từ cao đến thấp', value: {sb: 'originalPrice', sd: 'DESC'}}
];

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})

export class SearchResultsComponent extends BasePage implements OnInit {

  searchQuery: any;
  priceOptions: Array<any> = PRICE_OPTIONS;
  products: any[] = [];
  conf: any = {
    url: '',
    page: 1,
    limit: 12,
    sb: '',
    sd: '',
    totalItems: 100
  };
  title = '';
  searchFeature: any;

  @Input()
  set data(res: IResSearchContent) {
    if (res.status === HTTP_CODES.SUCCESS) {
      this.products = res.data.products;
      this.conf.totalItems = res.data.totalItems;
      this.searchQuery = res.data.searchQuery;
      this.title = SearchSelectorService.getTitleFromSearchQuery(this.searchQuery);
      this.searchFeature = SearchSelectorService.getSearchFeature(this.searchQuery);
      this.seoService.setHeader(this.title);
    }
  }

  @ViewChild('paginator') paginator: Paginator;

  constructor(private searchService: SearchService,
              private activatedRoute: ActivatedRoute,
              private seoService: SeoService,
              private router: Router) {
    super();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.conf.url = this.getCurrentUrl();
    this._initBreadCrumb();
  }

  changePage(event: any) {
    this.conf.page = event.page + 1;
    this.fetchData();
  }

  onChangeSort(event: any) {
    Object.assign(this.conf, event.value.value);
    this.fetchData();
    this.setToFirstPage();
  }

  setToFirstPage() {
    if (this.paginator) {
      this.paginator.changePage(0);
      (this.paginator as any).cd.detectChanges();
    }
  }

  private getCurrentUrl(): string {
    return this.activatedRoute.snapshot.url
      .map(s => s.path)
      .join('/');
  }

  private fetchData() {
    const params = this.generateParams();
    const sub = this.searchService.searchContent(params)
      .subscribe((res: IResSearchContent) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.products = res.data.products;
          this.conf.totalItems = res.data.totalItems;
          this.searchQuery = res.data.searchQuery;
        }
      });

    this.subscriptions.push(sub);
  }

  private generateParams(): any {
    const params = {
      limit: this.conf.limit,
      page: this.conf.page,
      url: this.conf.url,
      sb: this.conf.sb,
      sd: this.conf.sd
    };

    return params;
  }

  onSearch(searchBoxParams: any) {
    const sub = this.searchService.searchBox(searchBoxParams)
      .subscribe((res: IResSearchBoxResult) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.router.navigate([`danh-muc/${res.data.url}`]);
        }
      });

    this.subscriptions.push(sub);
  }

  private _initBreadCrumb() {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: this.title,
        routerLink: this.router.url
      }
    ]);
  }

}
