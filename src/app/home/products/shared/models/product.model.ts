export interface Product {
  title: string;
  images: string[];
  originalPrice: number;
  sale?: boolean;
}