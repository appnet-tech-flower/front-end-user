import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { HomeService } from '../shared/services/home.service';
import { Product } from '../detail-product/shared/models/Product.model';
import { BasePage } from '../shared/components/base/base-page';
import { IResSearchBoxResult, SearchService } from '../shared/services/search.service';
import { SeoService } from '../shared/services/seo.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BasePage implements OnInit {
  featuredProducts: Product[] = [];
  saleProducts: Product[] = [];

  constructor(private _homeService: HomeService,
              private _searchService: SearchService,
              private _router: Router,
              private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    this.scrollTop();
    this.globalService.setBreadcrumbs([]);

    const sub = this._homeService.getProduct().subscribe(
      (res: any) => {
        this.featuredProducts = res.data.entries.featuredProducts;
        this.saleProducts = res.data.entries.saleProducts;
      }
    );
    this.subscriptions.push(sub);
    this.seoService.setHeader('');
  }

  onSearch(searchBoxParams: any) {
    const sub = this._searchService.searchBox(searchBoxParams)
      .subscribe((res: IResSearchBoxResult) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          console.log(this._router);
          this._router.navigate([`danh-muc/${res.data.url}`]);
        }
      });

    this.subscriptions.push(sub);
  }
}
