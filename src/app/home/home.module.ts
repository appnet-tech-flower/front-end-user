import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { HomeService } from '../shared/services/home.service';
import { SearchService } from '../shared/services/search.service';
import { HomeComponent } from './home.component';
import { ProductsFeaturedComponent } from './products/products-featured/products-featured.component';
import { ProductsPromotionComponent } from './products/products-promotion/products-promotion.component';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { ButtonModule, GalleriaModule } from 'primeng/primeng';
import { NewsListModule } from '../shared/components/news-list/news-list.module';

@NgModule({
  declarations: [
    HomeComponent,
    ProductsPromotionComponent,
    ProductsFeaturedComponent
  ],
  imports: [
    CommonModule,
    SearchBoxModule,
    ProductItemModule,
    RouterModule,
    ButtonModule,
    GalleriaModule,
    NewsListModule
  ],
  providers: [
    SearchService,
    HomeService
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule {

}
