import { NgModule } from '@angular/core';
import { ConfirmAccountComponent } from './confirm-account.component';
import { CardModule } from 'primeng/card';
import { MessageModule, MessagesModule, InputTextModule, ButtonModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ConfirmAccountComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    CardModule,
    MessagesModule,
    MessageModule,
    InputTextModule,
    ButtonModule
  ],
  providers: [
  ],
  exports: [
    ConfirmAccountComponent
  ]
})
export class ConfirmAccountModule {

}
