import { Component, OnInit } from '@angular/core';
import { SaleNotificationRegisterService } from './sale-notification-register.service';
import { EditableFormBaseComponent } from '../shared/components/base/editable-form-base.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-sale-notification-register',
  templateUrl: './sale-notification-register.component.html',
  styleUrls: ['./sale-notification-register.component.scss']
})
export class SaleNotificationRegisterComponent extends EditableFormBaseComponent implements OnInit {

  form: FormGroup;

  constructor(
    private _saleNotificationRegisterService: SaleNotificationRegisterService
  ) {
    super();
  }

  ngOnInit() {
    this._initForm();
  }

  private _initForm() {
    this.form = this.fb.group({
      email: ['', this.validatorService.checkEmail()]
    });
  }

  generatePostParams() {
    const params = { ...this.form.value };
    return params;
  }

  submitForm() {
    this.onSubmit();
  }

  post() {
    const params = this.generatePostParams();

    this.globalService.setLoadingSpinner(true);
    const sub = this._saleNotificationRegisterService.registerSaleNotification(params)
      .subscribe(res => {
        this.globalService.setLoadingSpinner(false);
        const message = res.messages.join('\n');
        this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: message });
      });
    this.subscriptions.push(sub);
  }

}
