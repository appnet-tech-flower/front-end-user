import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BasePage } from '../shared/components/base/base-page';
import { API } from '../shared/constants/api.constant';

export interface IRegisterSaleNotification {
  email: string;
}

@Injectable({
  providedIn: 'root'
})
export class SaleNotificationRegisterService extends BasePage {

  constructor(
    private _http: HttpClient,
  ) {
    super();
  }

  registerSaleNotification(params: IRegisterSaleNotification): Observable<any> {
    return this._http.post(API.SaleNotification.registerSaleNotification, params);
  }
}
