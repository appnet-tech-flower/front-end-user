import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleNotificationRegisterComponent } from './sale-notification-register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CardModule, ButtonModule, InputTextModule } from 'primeng/primeng';

@NgModule({
  declarations: [SaleNotificationRegisterComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CardModule,
    ButtonModule,
    InputTextModule
  ],
  exports: [
    SaleNotificationRegisterComponent
  ]
})
export class SaleNotificationRegisterModule { }
