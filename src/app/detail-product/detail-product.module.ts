import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DetailProductComponent } from './detail-product.component';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { GalleriaModule } from 'primeng/galleria';
import { SpinnerModule, ButtonModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsSimilarComponent } from './products-similar/products-similar.component';
import { RouterModule } from '@angular/router';


@NgModule({
  entryComponents: [DetailProductComponent],
  declarations: [DetailProductComponent, ProductsSimilarComponent],
  imports: [
    CommonModule,
    FormsModule,
    ProductItemModule,
    SearchBoxModule,
    GalleriaModule,
    SpinnerModule,
    ButtonModule,
    RouterModule
  ],
  exports: [DetailProductComponent]
})
export class DetailProductModule {

}
