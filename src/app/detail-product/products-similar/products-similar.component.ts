import { Component, Input, OnInit } from '@angular/core';
import { IProduct } from '../../sales-channel/list-product/list-product.component';

@Component({
  selector: 'app-products-similar',
  templateUrl: './products-similar.component.html',
  styleUrls: ['./products-similar.component.scss']
})
export class ProductsSimilarComponent implements OnInit {
  @Input() products: Array<IProduct> = [];

  constructor() {
  }

  ngOnInit(): void {

  }
}
