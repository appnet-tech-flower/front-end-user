import { Injectable } from '@angular/core';

import { API } from '../shared/constants/api.constant';
import { HttpClient } from '@angular/common/http';
import { Product } from '../detail-product/shared/models/Product.model';


export interface IResListNews {
  status: number;
  messages: string[];
  data: {
    meta: {
      totalItems?: number;
      limit?: number;
      page?: number;
    };
    news?: Product[];
    totalItems?: number;
    searchQuery?: any;
  };
}
@Injectable()
export class NewsService {

  constructor(private _http: HttpClient) {
  }

  listNews(params: any) {
    return this._http.get(API.New.ListNews, {params});
  }

  listHighlightNews() {
    return this._http.get(API.New.ListHighlight);
  }


  getDetail(slug: string) {
    const url = API.New.DetailNews.replace('{url}', slug);
    return this._http.get(url);
  }
}
