import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { environment } from '../../../environments/environment';
import { IResSearchBoxResult, SearchService } from '../../shared/services/search.service';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { SeoService } from '../../shared/services/seo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IResListNews, NewsService } from '../news.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})

export class DetailNewsComponent extends BasePage implements OnInit {

  _data: any = {
    image: 'assets/images/project-1.jpg',
    url: '',
    title: 'title tin tuc',
    content: 'content tin tuc',
    description: 'description tin tuc',
    createdAt: new Date(),
  };
  textEndPage = '';
  environment = environment;
  listNewsHighlight: any[] = [];
  searchQuery: any;
  constructor(private seoService: SeoService,
              private router: Router,
              private route: ActivatedRoute,
              private searchService: SearchService,
              private newsService: NewsService) {
    super();
    this.scrollTop();
  }

  ngOnInit(): void {
    const url = this.route.snapshot.paramMap.get('url');
    this.getDetail(url);
    this.getHighlight();
  }

  private _initBreadCrumb(news) {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Tin tức',
        routerLink: '/tin-tuc'
      },
      {
        label: news.title,
        routerLink: '/tin-tuc/' + news.url
      }
    ]);
  }

  onSearch(searchBoxParams: any) {
    const sub = this.searchService.searchBox(searchBoxParams)
      .subscribe((res: IResSearchBoxResult) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.router.navigate([`danh-muc/${res.data.url}`]);
        }
      });

    this.subscriptions.push(sub);
  }

  getDetail(url) {
    const sub = this.newsService.getDetail(url)
      .subscribe((res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this._data = res.data;
          this._initBreadCrumb(res.data);
        } else {
          this.router.navigate(['/']);
        }
      });
    this.subscriptions.push(sub);
  }

  getHighlight() {
    const sub = this.newsService.listHighlightNews()
      .subscribe((res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.listNewsHighlight = res.data;
        } else {
          this.router.navigate(['/']);
        }
      });
    this.subscriptions.push(sub);
  }

}
