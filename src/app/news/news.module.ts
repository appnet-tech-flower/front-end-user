import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news.component';
import { NewsRoutingModule } from './news-routing.module';
import { NewsService } from './news.service';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { CardModule, DropdownModule, PanelMenuModule, TabMenuModule, TabViewModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';
import { NewItemModule } from '../shared/components/new-item/new-item.module';
import { TextEndPageModule } from '../shared/components/text-end-page/text-end-page.module';
import { DetailNewsComponent } from './detail/detail.component';


@NgModule({
  declarations: [NewsComponent, DetailNewsComponent],
  imports: [
    CommonModule,
    NewsRoutingModule,
    CommonModule,
    TabMenuModule,
    PanelMenuModule,
    TabViewModule,
    FormsModule,
    PaginatorModule,
    ProductItemModule,
    DropdownModule,
    SearchBoxModule,
    CardModule,
    NewItemModule,
    TextEndPageModule
  ],
  providers: [
    NewsService
  ]
})
export class NewsModule { }
