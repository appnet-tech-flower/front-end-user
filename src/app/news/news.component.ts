import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IResSearchBoxResult, IResSearchContent, SearchService } from '../shared/services/search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { SeoService } from '../shared/services/seo.service';
import { Paginator } from 'primeng/primeng';
import { BasePage } from '../shared/components/base/base-page';
import { IResListNews, NewsService } from './news.service';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent extends BasePage implements OnInit {

  searchQuery: any;
  news: any[] = [];
  conf: any = {
    page: 1,
    limit: 12,
    sb: '',
    sd: '',
    totalItems: 100
  };

  @ViewChild('paginator') paginator: Paginator;

  constructor(private searchService: SearchService,
              private newsService: NewsService,
              private activatedRoute: ActivatedRoute,
              private seoService: SeoService,
              private router: Router) {
    super();
  }

  ngOnInit() {
    this.scrollTop();
    this.globalService.setBreadcrumbs([]);
    this.fetchData();
    this._initBreadCrumb();
  }

  changePage(event: any) {
    this.conf.page = event.page + 1;
    this.fetchData();
  }

  onChangeSort(event: any) {
    Object.assign(this.conf, event.value.value);
    this.setToFirstPage();
  }

  setToFirstPage() {
    this.paginator.changePage(0);
    (this.paginator as any).cd.detectChanges();
  }

  private fetchData() {
    const params = this.generateParams();
    const sub = this.newsService.listNews(params)
        .subscribe((res: IResListNews) => {
          if (res.status === HTTP_CODES.SUCCESS) {
            this.news = res.data.news;
            this.conf.totalItems = res.data.meta.totalItems;
            this.searchQuery = res.data.searchQuery;
          }
        });

    this.subscriptions.push(sub);
  }

  private generateParams(): any {
    const params = {
      limit: this.conf.limit,
      page: this.conf.page,
      sb: this.conf.sb,
      sd: this.conf.sd
    };
    if (params.sb === '') {
      delete params.sb;
    }

    if (params.sd === '') {
      delete params.sd;
    }

    return params;
  }

  onSearch(searchBoxParams: any) {
    const sub = this.searchService.searchBox(searchBoxParams)
        .subscribe((res: IResSearchBoxResult) => {
          if (res.status === HTTP_CODES.SUCCESS) {
            this.router.navigate([`danh-muc/${res.data.url}`]);
          }
        });

    this.subscriptions.push(sub);
  }

  private _initBreadCrumb() {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Tin tức',
        routerLink: '/tin-tuc'
      }
    ]);
  }

}
