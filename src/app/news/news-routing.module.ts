import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NewsComponent } from './news.component';
import {DetailNewsComponent} from './detail/detail.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      pathMatch: 'full',
      component: NewsComponent
    },
    {
      path: ':url',
      component: DetailNewsComponent
    }
  ])],
  exports: [RouterModule]
})
export class NewsRoutingModule {

}
