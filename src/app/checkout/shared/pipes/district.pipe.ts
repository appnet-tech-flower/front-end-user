import { Pipe, PipeTransform } from '@angular/core';
import { SearchSelector } from '../../../shared/constants/search-selector.constant';
import Cities = SearchSelector.Cities;

@Pipe({
  name: 'district'
})
export class DistrictPipe implements PipeTransform {
  transform(id: number, arg?: any): any {
    let districts: any[] = [];
    const cities = Cities;
    districts = cities.map(city => city.districts);
    for (const district of districts) {
      for (const item of district) {
        if (item.id === id) {
          return item.name;
        }
      }
    }
  }
}
