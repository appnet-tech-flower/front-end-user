import { NgModule } from '@angular/core';
import { MapModule } from '../shared/components/map/map.module';
import { CartComponent } from './cart/cart.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckoutRoutingModule } from './checkout-routing.module';
import { CardModule } from 'primeng/card';
import {
  ButtonModule,
  SpinnerModule,
  StepsModule,
  CalendarModule,
  InputMaskModule,
  InputTextModule,
  MessagesModule,
  MessageModule,
  DropdownModule,
  CheckboxModule,
  DialogModule, InputTextareaModule
} from 'primeng/primeng';
import { DeliveryComponent } from './delivery/delivery.component';
import { ToastModule } from 'primeng/toast';
import { CityPipe } from './shared/pipes/city.pipe';
import { DistrictPipe } from './shared/pipes/district.pipe';
import { WardPipe } from './shared/pipes/ward.pipe';
import { PaymentComponent } from './payment/payment.component';
import { PurchaseSuccessComponent } from './purchase-success/purchase-success.component';
import { AppStaticImageModule } from "../shared/pipes/app-static-image/app-static-image.module";

@NgModule({
  declarations: [CartComponent, DeliveryComponent, CityPipe, DistrictPipe, WardPipe, PaymentComponent, PurchaseSuccessComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CheckoutRoutingModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    CardModule,
    ButtonModule,
    SpinnerModule,
    StepsModule,
    CalendarModule,
    InputMaskModule,
    InputTextModule,
    DropdownModule,
    CheckboxModule,
    MapModule,
    DialogModule,
    InputTextareaModule,
    AppStaticImageModule
  ],
  exports: [CityPipe, DistrictPipe, WardPipe]
})
export class CheckoutModule {}
