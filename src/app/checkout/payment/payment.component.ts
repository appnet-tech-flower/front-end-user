import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { BasePage } from '../../shared/components/base/base-page';
import { GlobalService } from '../../shared/services/global.service';
import { Router } from '@angular/router';
import { IDeliveryInfo } from '../shared/models/delivery.model';
import { ConfirmationService } from 'primeng/api';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { IOrderItem } from '../../shared/services/order.service';
import * as moment from 'moment';
import { CookieService } from 'ngx-cookie-service';
import { Dialog } from 'primeng/dialog';
import { GlobalConstant } from '../../shared/constants/global.constant';
import SizeImage = GlobalConstant.SizeImage;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
  providers: [ConfirmationService]
})
export class PaymentComponent extends BasePage implements OnInit {
  checkedBankTransfer = false;
  checked = true;
  checkedCall = true;
  displayDialogFlag = false;
  items: MenuItem[];
  info: IDeliveryInfo;
  SizeImage = SizeImage;

  total: any;
  totalShippingCost: any;
  order: any;
  orderItems: IOrderItem[];

  @ViewChild('orderDialog') orderDialog: Dialog;

  constructor(public globalService: GlobalService,
              private cookieService: CookieService,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.globalService.setLoadingSpinner(false);
    this.scrollTop();
    this.getCurrentItemsOrder();
    this._initBreadCrumb();

    const sub = this.orderService.getDeliveryInfo().subscribe(
      (res: IDeliveryInfo) => {
        this.info = res;
        this.totalShippingCost = res.totalShippingCost;
      }
    );
    this.subscriptions.push(sub);
    this.items = [
      {label: 'Giao hàng'},
      {label: 'Thanh toán'},
      {label: 'Hoàn tất'}
    ];
  }

  getCurrentItemsOrder() {
    const sub = this.orderService.getProductsInCart()
      .subscribe(data => {
        this.orderItems = data;
        console.log(this.orderItems);
        this._updateTotal();
      });

    this.subscriptions.push(sub);
  }

  onBackToCart() {
    this._router.navigate(['/mua']);
  }

  onBackToDelivery() {
    this._router.navigate(['/mua/giao-hang']);
  }

  private _updateTotal() {
    this.total = this.orderItems.map(
      (item: any) => {
        if (item.product.saleOff.active) {
          return item.product.saleOff.price * item.quantity;
        }
        return item.product.originalPrice * item.quantity;
      }
    );

    const add = (calc, a) => {
      return calc + a;
    };

    this.total = this.total.reduce(add, 0);
  }

  onSubmitOrder() {
    if (!this.checkedBankTransfer) {
      return this.swalDialogService.openInfo('Bạn cần chọn phương thức thanh toán');
    }
    this.displayDialogFlag = true;
  }

  confirmOrder(isAccept: boolean) {
    if (isAccept) {
      this.handleSubmitOrder();
    } else {
      this.displayDialogFlag = false;
    }
  }

  private handleSubmitOrder() {
    if (this.authService.userInfo) {
      this.submitOrderCaseLogIn();
    } else {
      this.submitOrderCaseNotLogIn();
    }
  }

  private submitOrderCaseNotLogIn() {
    const data = {
      buyerInfo: {
        name: this.info.buyerName,
        email: this.info.buyerEmail,
        phone: this.info.buyerPhone,
      },
      receiverInfo: {
        name: this.info.name,
        phone: this.info.phone,
        address: this.info.address,
        longitude: this.info.longitude,
        latitude: this.info.latitude
      },
      items: this.orderItems.map(oi => {
        return {
          productId: oi.product._id,
          quantity: oi.quantity
        };
      }),
      deliveryTime: moment(this.info.deliveryTime).startOf('day').toDate(),
      expectedDeliveryTime: this.info.expectedDeliveryTime,
      contentOrder: this.info.contentOrder,
      note: this.info.note
    };

    this.globalService.setLoadingSpinner(true);
    const sub = this.orderService.submitOrderForGuest(data)
      .subscribe((res: any) => {
        this.handleAfterOrder(res);
      });

    this.subscriptions.push(sub);
  }

  private submitOrderCaseLogIn() {
    const data = {
      address: this.info._id,
      deliveryTime: moment(this.info.deliveryTime).startOf('day').toDate(),
      expectedDeliveryTime: this.info.expectedDeliveryTime,
      contentOrder: this.info.contentOrder,
      note: this.info.note
    };

    this.globalService.setLoadingSpinner(true);
    const sub = this.orderService.submitOrder(data).subscribe(
      (res: any) => {
        this.handleAfterOrder(res);
      }
    );

    this.subscriptions.push(sub);
  }

  private _initBreadCrumb() {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Đặt hàng',
        icon: 'pi pi-shopping-cart',
        routerLink: '/mua'
      },
      {
        label: 'Chọn ngày giờ, địa điểm giao hàng',
        icon: 'pi pi-map-marker',
        routerLink: '/mua/giao-hang'
      },
      {
        label: 'Mua hàng',
        icon: 'pi pi-inbox'
      }

    ]);
  }

  handleAfterOrder(response: any) {
    this.globalService.setLoadingSpinner(false);
    if (response.status === HTTP_CODES.SUCCESS) {
      this.orderService.setProductsInCarts([]); // reset cart
      this.cookieService.delete('productsCookie');
      this._router.navigate(['/mua/dat-hang-thanh-cong'], {queryParams: {id: response.data.code}});
    } else {
      this.messageService.add({
        key: 'payment-message',
        severity: 'error',
        summary: 'Thông Báo',
        detail: 'Không thể đặt hàng tại lúc này, xin quý khác vui lòng thử lại sau !',
        life: 10000
      });

      console.error(response);
    }
  }

  onShowDialog() {
    this.orderDialog.onWindowResize();
  }
}
